import { Injectable } from '@angular/core';
import { Location } from '@angular/common';

@Injectable()
export class AuthService {
	jwtToken
	constructor(
		private location: Location
	) { }

	sendValue(key: string,token: string) {
		localStorage.setItem(key, token)
	}
	
	getValue(key: string) {
		return localStorage.getItem(key)
	}

	isLoggedIn() {
		return this.getValue('token') !== null;
	}

	goBack() {
		this.location.back();
	}
}