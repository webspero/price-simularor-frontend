import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorprocedureeditComponent } from './vendorprocedureedit.component';

describe('VendorprocedureeditComponent', () => {
  let component: VendorprocedureeditComponent;
  let fixture: ComponentFixture<VendorprocedureeditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorprocedureeditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorprocedureeditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
