import { Component, OnInit } from '@angular/core';
import { MultiSelectAllModule } from '@syncfusion/ej2-angular-dropdowns';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiServiceService } from '../../api-service.service';
import { TokenexpiryService } from '../../tokenexpiry.service';
import * as $ from 'jquery'; 
import { Location } from '@angular/common';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-vendorprocedureedit',
  templateUrl: './vendorprocedureedit.component.html',
  styleUrls: ['./vendorprocedureedit.component.css']
})
export class VendorprocedureeditComponent implements OnInit {
	message;
	errormsg
	submitted = false;
	shhowGender = false;
	moduleList: any = [];
	module_type;
	genderCheck: any = [];
	area_interest: any = [];
	area_interest_val;
	procedure_gender;
	interestType;
	apikey;
	token;
	error_msg;
	procedureType;
	surgicalType;
	genderVal = "Both";
	surgeryType: any = [];
	procedureArray;

	description;
	diseaseType;
	procedure_cost_start;
	procedure_cost_end;
	procedure_cost_discount;
	param1;
	procdeureForm;
	editModule;
	updateForm:FormGroup;
	param2;
	successClass;
	area_interest_selected;
	GenderName;
	isDisabled: boolean = true
	userDetail;
	totalData:any
	gender=[
		{name:"Male"},
		{name:"Female"},
		{name:"Both"}
	]

	constructor(
		private activatedRoute: ActivatedRoute,
		private http: HttpClient,
		private router: Router,
		private apiService: ApiServiceService,
		private expireTokenService: TokenexpiryService,
		private _location: Location,
		public fb: FormBuilder
	) {
		this.updateForm = this.fb.group({
			module_type: [{ value:null,disabled:true }],
			area_interest_val: [{ value:null,disabled:true }],
			procedure_gender: [{ value:null,disabled:false }],
			procedure_type: [{ value:null,disabled:true }],
			diseaseType: [{ value:null,disabled:true }],
			procedure_description: ['', Validators.required],
			//procedure_disclaimer: ['Price are estimated cash / self-pay prices outsite of insurance.', Validators.required],
			procedure_cost_female: ['0', [Validators.required,Validators.pattern("^[0-9-]*$")]],
			procedure_cost_male: ['0', [Validators.required,Validators.pattern("^[0-9-]*$")]],
			procedure_cost_start: ['0', [Validators.pattern("^[0-9-]*$")]],
		});
		this.token = localStorage.getItem("token");
		this.apikey = localStorage.getItem("apikey");
		this.userDetail = JSON.parse(localStorage.getItem('userdetail'))
		this.activatedRoute.params.subscribe(params => {
			this.param1 = params['id'];
			this.getProcedure();
		});
	}

	ngOnInit() {}

	backClicked() {
		this._location.back();
	}

	getProcedure() {
		const body = {
			admin_id:this.userDetail.id,
			id:this.param1
		}
		this.apiService.getProcedurebyid(this.token,this.apikey, this.param1).subscribe((response: any) => {
			if (response.success == false && response.message == "Token is not valid") {
				this.expireTokenService.expireToken();
			} else if (response) {
				var typeSurgical;
				if(response[0].type){
					typeSurgical = response[0].type
				}
				this.totalData=response.response
				this.updateForm.patchValue({
					module_type: response[0].mod_name ,
					area_interest_val: response[0].cat_name,
					procedure_gender: response[0].prod_gender ,
					procedure_type: response[0].type,
					diseaseType: response[0].procedure_name,
					procedure_description: response[0].prod_desc,
					//procedure_disclaimer: ['Price are estimated cash / self-pay prices outsite of insurance.', Validators.required],
					procedure_cost_female: response[0].meta_val[1].meta_value,
					procedure_cost_male: response[0].meta_val[0].meta_value,
				})
				this.genderVal = response[0].prod_gender
				
				// this.callProcedure(this.totalData.module_id.toString(),this.totalData.procedureCatId.toString(),this.totalData.gender,this.totalData.type)
			} else {
				this.message = "Something went wrong!";
			}
		}, (err) => {
			console.log(err);
		})
	}

	callProcedure(module, interstType, gender, procedure) {
		this.apiService.getProcedure(this.token, this.apikey, module, interstType, gender, procedure).subscribe((response: any) => {
			if (response && response.length > 0) {
				this.procedureArray = response;
				var flags = [],
					output = [],
					l = this.procedureArray.length,
					i;
				for (i = 0; i < l; i++) {
					if (flags[this.procedureArray[i].type]) continue;
					flags[this.procedureArray[i].type] = true;
					if (this.procedureArray[i].type) {
						output.push(this.procedureArray[i].type);
					}
				}
				if (output.length > 0) {
					this.surgeryType = output;
				}
			} else {
				this.procedureArray = [];
				this.procdeureForm.controls['diseaseType'].setValue(null);
				this.description = '';
				this.surgeryType = "";
			}
		}, (err) => {
			console.log(err);
		})
	}

	onChangesurgicalType(val) {
		this.surgicalType = val;
		// this.callProcedure(this.totalData.module_id.toString(),this.totalData.procedureCatId.toString(),this.procdeureForm.value.procedure_gender, val);
	}

	onGenderChange(value){
		this.genderVal=value
		// this.callProcedure(this.totalData.module_id.toString(),this.totalData.procedureCatId.toString(),this.genderVal.toString(), this.procdeureForm.value.procedure_type);
	}

	get f() {
		return this.updateForm.controls;
	}

	//Submit Form
	logValue(formval) {
		this.submitted = true;
		if(this.updateForm.invalid) return
		var datasets = {};
		datasets = {
			//procedure_disclaimer: formval.procedure_disclaimer,
			procedure_cost_male: formval.procedure_cost_male,
			procedure_cost_female: formval.procedure_cost_female,
			procedure_desc: formval.procedure_description,
			procedure_gender:this.updateForm.value.procedure_gender
		}
		this.apiService.updateProcedure(datasets, this.userDetail.token,this.param1,this.apikey).subscribe((response: any) => {
			if (response.success == false && response.message == "Token is not valid") {
				this.expireTokenService.expireToken();
			} else if (response.status == 200) {
				this.successClass = 'alert-success';
				//this.message = "Updated Successfully!";
				Swal.fire(
					'Success!',
					"Updated successfully!",
					'success'
				)
				setTimeout(() => {
					if (this.userDetail.userType === 'superadmin') {
						this.router.navigate(['vendorProcedure']);
					} else {
						this.router.navigate(['view']);
					}
				}, 1500);

			} else {
				this.message = "Something went wrong!";
			}
		}, (err) => {
			console.log(err);
		})
	}

}
