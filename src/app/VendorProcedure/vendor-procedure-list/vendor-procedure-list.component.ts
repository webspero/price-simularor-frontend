import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { ApiServiceService } from '../../api-service.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { TokenexpiryService } from '../../tokenexpiry.service';
import { JsonAdaptor } from '@syncfusion/ej2-data';
import { Location } from '@angular/common';
import { ExportToCsv } from 'export-to-csv';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-vendor-procedure-list',
  templateUrl: './vendor-procedure-list.component.html',
  styleUrls: ['./vendor-procedure-list.component.css']
})
export class VendorProcedureListComponent implements OnInit {
	token;
	apikey;
	procedure_array:any=[];
	RealTime:any=[];
	allModule;
	reply;
	updateDiscount;
	modalRef: BsModalRef;
	activePage:number=1
	range:number=10
	totalCount:any
	totalData:any;
	public procedureName:string='';
	public vendorName:string='';
	public apiRefresh:Boolean = false
	public options = { 
		fieldSeparator: ',',
		quoteStrings: '"',
		decimalSeparator: '.',
		showLabels: true, 
		showTitle: false,
		filename:'data_download',
		useTextFile: false,
		useBom: true,
		useKeysAsHeaders: true
    };
	public selectType =[
		{name:'Vendor Name',value:'vendor'},
		{name:'Procedure Name',value:'procedure'}
	]
	
	public csvExporter = new ExportToCsv(this.options);

	constructor(
		private router: Router,
		private modalService: BsModalService,
		private apiService: ApiServiceService,
		private formBuilder: FormBuilder,
		private expireTokenService: TokenexpiryService,
		private _location: Location

	) {
		this.token = localStorage.getItem("token");
		this.apikey = localStorage.getItem("apikey");
		this.getVendorProcedureList();
		this.moduleList();
		this.updateDiscount = this.formBuilder.group({
		  discountval:['', [Validators.min(1),Validators.required]],
		})
    }

	ngOnInit() {
		this.apiService.currentUser(this.token,this.apikey).subscribe((response: any) => {
			if (response.success == false && response.message == "Token is not valid") {
				this.expireTokenService.expireToken();
			} else if(response.length > 0){
				this.updateDiscount.controls['discountval'].setValue(response[0].discount_rate);
			}
		}, (err) => {
			console.log(err);
		})
	}

	ngDoCheck(){
		if(this.apiRefresh){
			this.getVendorProcedureList()
			this.apiRefresh=false
		}
	}

	pageChanged(data){
		this.activePage=data
		this.getVendorProcedureList()
	}
  
	backClicked() {
		this._location.back();
	}

	getVendorProcedureList() { 
		this.apiService.getVandorProcedureList(this.token,this.activePage.toString(),this.range.toString()).subscribe((response: any) => {
			this.procedure_array = response.totalData;
			this.RealTime = response.totalData;
			this.totalCount = response.totalCount
			this.totalData = response.totalData
		}, (err) => {
			console.log(err);
		})
	}

	rate_show(values){
		var explodeVal = values.split(",");
		var maleCost = "For Male USD $"+explodeVal[0];
		var femaleCost = "For Female USD $"+explodeVal[1];
		return maleCost+"<br>"+femaleCost;
	}

	Excelrate_show(values){
		var explodeVal = values.split(",");
		var maleCost = "For Male USD $"+explodeVal[0];
		var femaleCost = "For Female USD $"+explodeVal[1];
		return maleCost+" , "+femaleCost;
	}


	editProcedure(array){
		var arrayData = JSON.stringify(array);
		this.router.navigate(['addProcedure']);
	}

  addProcedure(){
    this.router.navigate(['addProcedure']);
  }

  moduleList(value='all'){
    this.apiService.getModule(this.token,this.apikey,value).subscribe((response: any) => {
        this.allModule = response;
      }, (err) => {
        console.log(err);
    });
  }

  modulebyId(val){
    if(this.allModule){
      for(var k=0;k<this.allModule.length;k++){
        if(this.allModule[k].id == val){
          return this.allModule[k].module_name;
        }
      }
    }
  }



  deleteProcedures(id_val){
    var id = {id_val};
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to delete this record!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.apiService.deleteProcedure(id,this.token).subscribe((data)=>{
          Swal.fire(
            'Deleted!',
            'Your record has been deleted.',
            'success'
          )
          this.getVendorProcedureList();
        },(error)=>{

        },()=>{

        })
      // For more information about handling dismissals please visit
      // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'Your record is safe :)',
          'error'
        )
      }
    })
  }

  // deleteProcedures(id_val) {
  //   var id = {id_val};
  //   if (confirm('Are you sure you want to delete this?')) {
  //     this.apiService.deleteProcedure(id,this.token).subscribe((response: any) => {
  //       if (response.success == false && response.message == "Token is not valid") {
  //         this.expireTokenService.expireToken();
  //       } else {
  //         if(response.status == 200){
  //           this.getVendorProcedureList();
  //         }
  //       }
  //     }, (err) => {
  //       console.log(err);
  //     })
  //   }
    
  // }

    discountForm(disocuntForm: TemplateRef<any>) {
      this.modalRef = this.modalService.show(disocuntForm);
    } 

    submitDiscount(value){
      this.apiService.discount(this.token,this.apikey,value).subscribe((response: any) => {
          this.updateDiscount.controls['discountval'].setValue(response.response);
          this.reply = "Discount rate Updated";
          this.modalRef.hide();
          this.reply = '';
        }, (err) => {
          console.log(err);
      });
    }

    ApiRefresh(refresh){
      this.apiRefresh=refresh
    }
  
    displayActivePage(activePageNumber:number){  
      this.activePage = activePageNumber;
    } 

    	formatDate(date) {
		var d = new Date(date),
		month = '' + (d.getMonth() + 1),
		day = '' + d.getDate(),
		year = d.getFullYear();
		const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;
		
		return {"format_1":[year, month, day].join('-'),"format_2":[monthNames[d.getMonth()], day, year].join(', ')};
	}
	 

    getExcel(value){
      if(value){
        var _today = new Date();
        var today = this.formatDate(_today);
        this.options.filename = value+'_' +today.format_1;
        this.csvExporter = new ExportToCsv(this.options)
        var excelData = []
        if(this.totalData.length > 0){
          for(var i=0;i<this.totalData.length;i++){
            excelData.push({"S.NO":i+1,"NAME OF PRACTICE/BUSINESS":this.totalData[i].name_of_point,"MODULE NAME":this.totalData[i].procedure_module_name,"CATEGORY NAME":this.totalData[i].procedure_category_name,"PROCEDURE NAME":this.totalData[i].procedure_name,"PRICE":this.Excelrate_show(this.totalData[i].prod_cost)})
          }
        }
        if(excelData.length > 0){
          this.csvExporter.generateCsv(excelData);
        }
      }
  
    }

}
