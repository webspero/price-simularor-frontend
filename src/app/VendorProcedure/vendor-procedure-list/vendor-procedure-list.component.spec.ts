import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorProcedureListComponent } from './vendor-procedure-list.component';

describe('VendorProcedureListComponent', () => {
  let component: VendorProcedureListComponent;
  let fixture: ComponentFixture<VendorProcedureListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorProcedureListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorProcedureListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
