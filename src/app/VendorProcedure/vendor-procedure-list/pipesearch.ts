import {Pipe, PipeTransform } from '@angular/core';
import { VendorProcedureListComponent } from '../../VendorProcedure/vendor-procedure-list/vendor-procedure-list.component';
@Pipe({
	name: 'procedureSearch'
})
export class ProcedureSearchPipe implements PipeTransform {
	filterdata:any= [];

	constructor(private procedures: VendorProcedureListComponent) { }
	transform(items: Array<any>, vendorName: string,procedureName: string){
		this.filterdata = [];
		if (items && items.length){
			var returnVal = items.filter(item =>{
				if (vendorName && item.name_of_point.toLowerCase().indexOf(vendorName.toLowerCase()) === -1){
					return false;
				}
				
				if (procedureName && item.procedure_name.toLowerCase().indexOf(procedureName.toLowerCase()) === -1){
					return false;
				}
				return true;
				
		  	});
			if (vendorName || procedureName){
				this.procedures.activePage = 1;
			}
			
		  	this.procedures.totalData = returnVal;
		  	this.procedures.totalCount = returnVal.length;
		  	

			return returnVal;
		}
		else{
			return items;
		}
	}
}