import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../../api-service.service';
import { TokenexpiryService } from '../../tokenexpiry.service';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { environment } from '../../../environments/environment';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  images;
  selectedItem = 1
  token;
  api_key;
  message;
  CurrentUser;
  user_email;
  doctype;
  editProfileForm:FormGroup;
  successClass;
  submitted;
  isSubmit:boolean=false
  module_name;
  profileShow:boolean=true;
  profileimg;
  States = [
    {'id':'1', 'value': 'Alabama'},
    {'id':'2', 'value': 'Alaska'},
    {'id':'3', 'value': 'Arizona'},
    {'id':'4', 'value': 'Arkansas'},
    {'id':'5', 'value': 'California'},
    {'id':'6', 'value': 'Colorado'},
    {'id':'7', 'value': 'Connecticut'},
    {'id':'8', 'value': 'Delaware'},
    {'id':'9', 'value': 'District of Columbia'},
    {'id':'10', 'value': 'Florida'},
    {'id':'11', 'value': 'Georgia'},
    {'id':'12', 'value': 'Hawaii'},
    {'id':'13', 'value': 'Idaho'},
    {'id':'14', 'value': 'Illinois'},
    {'id':'15', 'value': 'Indiana'},
    {'id':'16', 'value': 'Iowa'},
    {'id':'17', 'value': 'Kansas'},
    {'id':'18', 'value': 'Kentucky'},
    {'id':'19', 'value': 'Louisiana'},
    {'id':'20', 'value': 'Maine'},
    {'id':'21', 'value': 'Maryland'},
    {'id':'22', 'value': 'Massachusetts'},
    {'id':'23', 'value': 'Michigan'},
    {'id':'24', 'value': 'Minnesota'},
    {'id':'25', 'value': 'Mississippi'},
    {'id':'26', 'value': 'Missouri'},
    {'id':'27', 'value': 'Montana'},
    {'id':'28', 'value': 'Nebraska'},
    {'id':'29', 'value': 'Nevada'},
    {'id':'30', 'value': 'New Hampshire'},
    {'id':'31', 'value': 'New Jersey'},
    {'id':'32', 'value': 'New Mexico'},
    {'id':'33', 'value': 'New York'},
    {'id':'34', 'value': 'North Carolina'},
    {'id':'35', 'value': 'North Dakota'},
    {'id':'36', 'value': 'Ohio'},
    {'id':'37', 'value': 'Oklahoma'},
    {'id':'38', 'value': 'Oregon'},
    {'id':'39', 'value': 'Pennsylvania'},
    {'id':'40', 'value': 'Rhode Island'},
    {'id':'41', 'value': 'South Carolina'},
    {'id':'42', 'value': 'South Dakota'},
    {'id':'43', 'value': 'Tennessee'},
    {'id':'44', 'value': 'Texas'},
    {'id':'45', 'value': 'Utah'},
    {'id':'46', 'value': 'Vermont'},
    {'id':'47', 'value': 'Virginia'},
    {'id':'48', 'value': 'Washington'},
    {'id':'49', 'value': 'West Virginia'},
    {'id':'50', 'value': 'Wisconsin'},
    {'id':'51', 'value': 'Wyoming'}
  ];
  showloader:boolean=false;
  userDetail : any
  key : string
  constructor(
     private apiService:ApiServiceService,
     private expireTokenService:TokenexpiryService,
     private router: Router,
     private activatedRoute: ActivatedRoute,
     private _location: Location

    ) {
    this.token = localStorage.getItem("token");
    // this.api_key = localStorage.getItem("apikey");
    this.key = this.activatedRoute.snapshot.paramMap.get('key');	
    this.userDetail = JSON.parse(localStorage.getItem('userdetail')) 
    this.get_CurrentUser();
    this.editProfileForm = new FormGroup({
      module_name:  new FormControl(''),
      first_name:  new FormControl(null, Validators.required),
      last_name:  new FormControl(null, Validators.required),
      name_of_point:  new FormControl(null, Validators.required),
      email_of_point:  new FormControl(null, Validators.required,),
      name_of_point_admin:  new FormControl(null, Validators.required),
      email_of_point_admin:  new FormControl('', Validators.required),
      provider_business_email:  new FormControl('', Validators.required),
      mobile:  new FormControl('', Validators.required),
      user_email:  new FormControl(null),
      providerurl:  new FormControl(null, Validators.required),
      street_address:  new FormControl(null, Validators.required),
      city:  new FormControl(null, Validators.required),
      country:new FormControl('usa', Validators.required),
      state:  new FormControl(null, Validators.required),
      zipcode:  new FormControl(null, Validators.required)
    });
   }
   get f() {
      return this.editProfileForm.controls;
  }

  backClicked() {
    this._location.back();
  }

  ngOnInit() {
  }
  get_CurrentUser(){
		this.apiService.currentUser(this.token,this.key).subscribe((response:any)=>{
			if(response.success == false && response.message == "Token is not valid") {
				this.expireTokenService.expireToken();
			} else if(response.length > 0 ) {
          this.CurrentUser = response[0];
          if(response[0].profile_image){
            this.profileimg = environment.backendUrl+''+response[0].profile_image;
          }else{
            this.profileimg = environment.backendUrl+'uploads/assets/img/default.png';
          }

          // this.editProfileForm.patchValue({
          //   module_name:  response[0].surgeon_name,
          //   first_name:  response[0].first_name,
          //   last_name:  response[0].last_name,
          //   name_of_point:  response[0].name_of_point,
          //   email_of_point:  response[0].email_of_point,
          //   name_of_point_admin:  response[0].name_of_point_admin,
          //   email_of_point_admin:  response[0].email_of_point_admin,
          //   provider_business_email: response[0].provider_business_email,
          //   mobile:  response[0].mobile,
          //   user_email:  response[0].email,
          //   providerurl: response[0].providerurl,
          //   street_address: response[0].street_address,
          //   city:  response[0].city,
          //   country:response[0].country,
          //   state:  response[0].state,
          //   zipcode:  response[0].zipcode
          // })

          this.editProfileForm.controls['module_name'].setValue(response[0].surgeon_name, {onlySelf: true});
          this.editProfileForm.controls['first_name'].setValue(response[0].first_name, {onlySelf: true});
          this.editProfileForm.controls['last_name'].setValue(response[0].last_name, {onlySelf: true});
          this.editProfileForm.controls['name_of_point'].setValue(response[0].name_of_point, {onlySelf: true});
          this.editProfileForm.controls['email_of_point'].setValue(response[0].email_of_point, {onlySelf: true});
          this.editProfileForm.controls['name_of_point_admin'].setValue(response[0].name_of_point_admin, {onlySelf: true});
          this.editProfileForm.controls['email_of_point_admin'].setValue(response[0].email_of_point_admin, {onlySelf: true});
          this.editProfileForm.controls['provider_business_email'].setValue(response[0].provider_business_email, {onlySelf: true});
          this.editProfileForm.controls['mobile'].setValue(response[0].mobile, {onlySelf: true});
          this.editProfileForm.controls['user_email'].setValue(response[0].email, {onlySelf: true});
          this.editProfileForm.controls['providerurl'].setValue(response[0].providerurl, {onlySelf: true});
          this.editProfileForm.controls['street_address'].setValue(response[0].street_address, {onlySelf: true});
          this.editProfileForm.controls['city'].setValue(response[0].city, {onlySelf: true});
           this.editProfileForm.controls['state'].setValue(response[0].state, {onlySelf: true});

          this.editProfileForm.controls['country'].setValue(this.editProfileForm.controls['country'].value, {onlySelf: true});
          this.editProfileForm.controls['zipcode'].setValue(response[0].zipcode, {onlySelf: true});

         //console.log(this.CurrentUser);
}else{
        this.message = "Something went worng";
}
},(err)=>{
    console.log(err);
    this.message = "Something went worng";
});  
  }


  showDiv(val,data){
    this.selectedItem = data
    this.profileShow = val
  }

  selectImage(event) {
    if (event.target.files.length > 0) {
			const file = event.target.files[0];
			this.images = file;
			var that = this
			var reader = new FileReader();
			reader.onloadend = function() {
				that.profileimg = reader.result
			}
			reader.readAsDataURL(file);
		}
  }

  onSubmit(){
    this.showloader = true;
    const formData = new FormData();
    formData.append('file', this.images);

    this.apiService.uploadUsermeta(this.token,this.key,formData).subscribe(
      (res:any) => {
        if(res.status === 200 ){
          this.showloader = false;
          this.profileimg = environment.backendUrl+''+res.results[0].profile_image;
        }else{
          this.showloader = false;
          this.profileimg = environment.backendUrl+'uploads/assets/img/profile.jpg';            
        }
      },
      (err) => {
        this.showloader = false;
        console.log(err);
    })
  }
 
  Save_Edited_Profile(editProfileForm){
    console.log(editProfileForm)
    console.log(editProfileForm.value)
    if(this.isSubmit) return;
     this.submitted = true;
     this.isSubmit = true
    if(this.editProfileForm.valid){
     this.apiService.UpdateUser(editProfileForm.value,this.token,this.key).subscribe((response:any)=>{
            if(response.length > 0 ) {
                this.CurrentUser = response[0];
                this.message = "User Details Updated";
                this.successClass = 'alert-success';
                this.isSubmit = false
                // localStorage.setItem("username", response[0]['first_name']+' '+response[0]['last_name']);
                this.backClicked()
              //   this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
              //       this.router.navigate(['manage-profile']);
              // }); 
                //this.router.navigate(['manage-profile']);
                Swal.fire(
                  'Success!',
                  'Successfully updated',
                  'success'
                )
      }else{
              this.isSubmit = false
              this.message = "Something went worng";
              this.successClass = 'alert-danger';
      }
      },(err)=>{
            this.isSubmit = false
            this.message = "Something went worng";
            this.successClass = 'alert-danger';
      });
    }
  }
}