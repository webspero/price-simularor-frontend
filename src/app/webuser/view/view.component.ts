import { Component, OnInit } from '@angular/core';
import { Route, ActivatedRoute} from '@angular/router'
import { ApiServiceService } from '../../api-service.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  States = [
    {'id':'1', 'value': 'Alabama'},
    {'id':'2', 'value': 'Alaska'},
    {'id':'3', 'value': 'Arizona'},
    {'id':'4', 'value': 'Arkansas'},
    {'id':'5', 'value': 'California'},
    {'id':'6', 'value': 'Colorado'},
    {'id':'7', 'value': 'Connecticut'},
    {'id':'8', 'value': 'Delaware'},
    {'id':'9', 'value': 'District of Columbia'},
    {'id':'10', 'value': 'Florida'},
    {'id':'11', 'value': 'Georgia'},
    {'id':'12', 'value': 'Hawaii'},
    {'id':'13', 'value': 'Idaho'},
    {'id':'14', 'value': 'Illinois'},
    {'id':'15', 'value': 'Indiana'},
    {'id':'16', 'value': 'Iowa'},
    {'id':'17', 'value': 'Kansas'},
    {'id':'18', 'value': 'Kentucky'},
    {'id':'19', 'value': 'Louisiana'},
    {'id':'20', 'value': 'Maine'},
    {'id':'21', 'value': 'Maryland'},
    {'id':'22', 'value': 'Massachusetts'},
    {'id':'23', 'value': 'Michigan'},
    {'id':'24', 'value': 'Minnesota'},
    {'id':'25', 'value': 'Mississippi'},
    {'id':'26', 'value': 'Missouri'},
    {'id':'27', 'value': 'Montana'},
    {'id':'28', 'value': 'Nebraska'},
    {'id':'29', 'value': 'Nevada'},
    {'id':'30', 'value': 'New Hampshire'},
    {'id':'31', 'value': 'New Jersey'},
    {'id':'32', 'value': 'New Mexico'},
    {'id':'33', 'value': 'New York'},
    {'id':'34', 'value': 'North Carolina'},
    {'id':'35', 'value': 'North Dakota'},
    {'id':'36', 'value': 'Ohio'},
    {'id':'37', 'value': 'Oklahoma'},
    {'id':'38', 'value': 'Oregon'},
    {'id':'39', 'value': 'Pennsylvania'},
    {'id':'40', 'value': 'Rhode Island'},
    {'id':'41', 'value': 'South Carolina'},
    {'id':'42', 'value': 'South Dakota'},
    {'id':'43', 'value': 'Tennessee'},
    {'id':'44', 'value': 'Texas'},
    {'id':'45', 'value': 'Utah'},
    {'id':'46', 'value': 'Vermont'},
    {'id':'47', 'value': 'Virginia'},
    {'id':'48', 'value': 'Washington'},
    {'id':'49', 'value': 'West Virginia'},
    {'id':'50', 'value': 'Wisconsin'},
    {'id':'51', 'value': 'Wyoming'}
  ];
  constructor(private activatedRoute : ActivatedRoute,private apiservice:ApiServiceService,private _location: Location) { }

  public key : string
  public webUserData : any
  public userDetail : any
  public showloader : boolean = false

  ngOnInit() {
    this.key = this.activatedRoute.snapshot.paramMap.get('key');	
    this.userDetail = JSON.parse(localStorage.getItem('userdetail')) 
    this.getWebUser()
  }

  selectState(value){
var index = this.States.findIndex(ele=>ele.id === value)
return this.States[index].value
}

  backClicked() {
		this._location.back();
	}

  getWebUser(){
    this.showloader= true
    this.apiservice.getWebuserbyid(this.userDetail.token,this.key).subscribe((data : any)=>{
        this.webUserData = data.results
        this.showloader= false
    },(error)=>{
      this.showloader= false
      console.log(error)
    },()=>{
      this.showloader= false
    })
  }

}
