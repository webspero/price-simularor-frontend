import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../../api-service.service';
import Swal from 'sweetalert2'
import { ExportToCsv } from 'export-to-csv';
import * as moment from 'moment';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
States = [
    {'id':'1', 'value': 'Alabama'},
    {'id':'2', 'value': 'Alaska'},
    {'id':'3', 'value': 'Arizona'},
    {'id':'4', 'value': 'Arkansas'},
    {'id':'5', 'value': 'California'},
    {'id':'6', 'value': 'Colorado'},
    {'id':'7', 'value': 'Connecticut'},
    {'id':'8', 'value': 'Delaware'},
    {'id':'9', 'value': 'District of Columbia'},
    {'id':'10', 'value': 'Florida'},
    {'id':'11', 'value': 'Georgia'},
    {'id':'12', 'value': 'Hawaii'},
    {'id':'13', 'value': 'Idaho'},
    {'id':'14', 'value': 'Illinois'},
    {'id':'15', 'value': 'Indiana'},
    {'id':'16', 'value': 'Iowa'},
    {'id':'17', 'value': 'Kansas'},
    {'id':'18', 'value': 'Kentucky'},
    {'id':'19', 'value': 'Louisiana'},
    {'id':'20', 'value': 'Maine'},
    {'id':'21', 'value': 'Maryland'},
    {'id':'22', 'value': 'Massachusetts'},
    {'id':'23', 'value': 'Michigan'},
    {'id':'24', 'value': 'Minnesota'},
    {'id':'25', 'value': 'Mississippi'},
    {'id':'26', 'value': 'Missouri'},
    {'id':'27', 'value': 'Montana'},
    {'id':'28', 'value': 'Nebraska'},
    {'id':'29', 'value': 'Nevada'},
    {'id':'30', 'value': 'New Hampshire'},
    {'id':'31', 'value': 'New Jersey'},
    {'id':'32', 'value': 'New Mexico'},
    {'id':'33', 'value': 'New York'},
    {'id':'34', 'value': 'North Carolina'},
    {'id':'35', 'value': 'North Dakota'},
    {'id':'36', 'value': 'Ohio'},
    {'id':'37', 'value': 'Oklahoma'},
    {'id':'38', 'value': 'Oregon'},
    {'id':'39', 'value': 'Pennsylvania'},
    {'id':'40', 'value': 'Rhode Island'},
    {'id':'41', 'value': 'South Carolina'},
    {'id':'42', 'value': 'South Dakota'},
    {'id':'43', 'value': 'Tennessee'},
    {'id':'44', 'value': 'Texas'},
    {'id':'45', 'value': 'Utah'},
    {'id':'46', 'value': 'Vermont'},
    {'id':'47', 'value': 'Virginia'},
    {'id':'48', 'value': 'Washington'},
    {'id':'49', 'value': 'West Virginia'},
    {'id':'50', 'value': 'Wisconsin'},
    {'id':'51', 'value': 'Wyoming'}
  ];
  public userDetail : any 
  public webUser :any
  public activePage:number = 1; 
	public totalCount:number = 0; 
  public range:number = 10
  public apiRefresh:Boolean = false
  public search : string = ''
  public searchType : string = 'email'
  public selectType =[
		{name:'Email',value:'email'},
    {name:'Speciality',value:'speciality'},
    {name:'City',value:'city'},
    {name:'State',value:'state'},
    {name:'Zipcode',value:'zipcode'}
  ]
  public dataLoading:boolean=false
  public totalData:any
  public options = { 
		fieldSeparator: ',',
		quoteStrings: '"',
		decimalSeparator: '.',
		showLabels: true, 
		showTitle: false,
		filename:'data_download',
		useTextFile: false,
		useBom: true,
		useKeysAsHeaders: true
  };
  public csvExporter = new ExportToCsv(this.options);

  constructor(private apiService:ApiServiceService) { }

  ngOnInit() {
    this.userDetail = JSON.parse(localStorage.getItem('userdetail'))
    this.getWebuser() 
  }

  getStates(value){
    var index = this.States.findIndex(ele=>ele.id === value)
return this.States[index].value
  }

  ngDoCheck(){
		if(this.apiRefresh){
			this.getWebuser()
			this.apiRefresh=false
		}
	}

  pageChanged(data){
    this.activePage=data
    this.getWebuser()
  }
	ApiRefresh(refresh){
		this.apiRefresh=refresh
	}

  displayActivePage(activePageNumber:number){  
		this.activePage = activePageNumber;
  }

  get_Date(val){
    var createDate = new Date(val);
    var dd = String(createDate.getDate()).padStart(2, '0');
    var mm = String(createDate.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = createDate.getFullYear();
    return mm + '/' + dd + '/' + yyyy;
  }
  
  getWebuser(){
    this.dataLoading=true
		this.apiService.getWebuser(this.userDetail.token,this.activePage.toString(),this.range.toString(),this.search.toString(),this.searchType.toString()).subscribe((data:any)=>{
      if(data.results){
        this.webUser= data.totalData
        this.totalCount=data.totalCount
        this.totalData =  data.totalData
        this.dataLoading=false
      }else{
        this.webUser= []
        this.totalCount=0
        this.totalData =  []
        this.dataLoading=false
      }
      
		},(error)=>{
			console.log(error)
		})
  }
  
   deleteWebuser(key){
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to delete this record!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.apiService.deleteWebuserbyid(this.userDetail.token,key).subscribe((data)=>{
          this.getWebuser() 
          Swal.fire(
            'Deleted!',
            'Your record has been deleted.',
            'success'
          )
        },(error)=>{

        },()=>{

        })
      // For more information about handling dismissals please visit
      // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'Your record is safe :)',
          'error'
        )
      }
    })
  }

  selectSearchType(type){
    this.searchType = type
    if(this.search !== ""){
      this.getWebuser()
    }
  }

  onChange(value){
    this.search = value
    this.getWebuser()
  }

  formatDate(date) {
		var d = new Date(date),
		month = '' + (d.getMonth() + 1),
		day = '' + d.getDate(),
		year = d.getFullYear();
		const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;
		
		return {"format_1":[year, month, day].join('-'),"format_2":[monthNames[d.getMonth()], day, year].join(', ')};
	}
	 

    getExcel(value){
      if(value){
        var _today = new Date();
        var today = this.formatDate(_today);
        this.options.filename = value+'_' +today.format_1;
        this.csvExporter = new ExportToCsv(this.options)
      
        // if(this.totalData.length > 0){
        //   this.csvExporter.generateCsv(this.totalData);
        // }

        var excelData = []

        if(this.totalData.length > 0){
          for(var i=0;i<this.totalData.length;i++){
            excelData.push({"S.NO":i+1,"FIRST NAME":this.totalData[i].first_name,"LAST NAME":this.totalData[i].last_name,"BUSINESS EMAIL":this.totalData[i].provider_business_email,"SPECIALITY":this.totalData[i].doc_type,"CITY":this.totalData[i].city,"STATE":this.totalData[i].state,"ZIPCODE":this.totalData[i].zipcode,"REGISTER DATE":this.get_Date(this.totalData[i].created_at)})
          }
        }
        if(excelData.length > 0){
          this.csvExporter.generateCsv(excelData);
        }
      }
    }

    date_change(date){
      const formatedDate = moment(date).format('MM/DD/YYYY')
      return formatedDate
    }
}