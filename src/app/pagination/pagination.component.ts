import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ShareService } from '../share.service'
@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {

  @Input() totalRecords: number = 0;  
  @Input() recordsPerPage: number = 0;
  @Input() page : number = 1 

  @Output() onPageChange: EventEmitter<number> = new EventEmitter();  
  @Output() onPageRefresh: EventEmitter<boolean> = new EventEmitter();  
  public pager: any = {};
  public pagedItems: any[];
  public allItems: any = [];

  public pages: number [] = [];  
  activePage:number;
  public maxpage:number=10
  constructor(public share : ShareService) { }

  ngOnInit() {
    setTimeout(() => {
      this.changePage(1) 
    }, 500);
    
  }

  ngOnChanges(){  
    const pageCount = this.getPageCount();  
    this.pages = this.getArrayOfPage(pageCount);  
    this.activePage = 1;  
    // this.onPageChange.emit(1); 
    
  }  

  private  getPageCount(): number {  
    let totalPage:number = 0;  
      
    if(this.totalRecords > 0 && this.recordsPerPage > 0){  
      const pageCount = this.totalRecords / this.recordsPerPage;  
      const roundedPageCount = Math.floor(pageCount);  

      totalPage = roundedPageCount < pageCount ? roundedPageCount + 1 : roundedPageCount;  
    }  

    return totalPage;  
  }  

  private getArrayOfPage(pageCount : number) : number [] {  
    let pageArray : number [] = [];  

    if(pageCount > 0){  
        for(var i=1 ; i<= pageCount ; i++){  
          pageArray.push(i);  
        }  
    }  

    return pageArray;  
  }  

  onClickPage(pageNumber:number){  
    if(pageNumber < 1) return;
    if(pageNumber > this.pages.length) return;
    this.activePage = pageNumber;  
    this.onPageChange.emit(this.activePage);  
    this.onPageRefresh.emit(true);  
}  


changePage(page: number) {
  this.onPageChange.emit(page)
  this.onPageRefresh.emit(false)
  this.activePage = page
  // get pager object from service
  this.pager = this.share.paginate(this.totalRecords, page, this.recordsPerPage,this.maxpage);
  
  // get current page of items
  this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
}

changePageNo(page: number) {
  this.onPageChange.emit(page)
  this.onPageRefresh.emit(true)
  this.activePage = page
  // get pager object from service
  this.pager = this.share.paginate(this.totalRecords, page, this.recordsPerPage,this.maxpage);
  // get current page of items
  this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
}

}
