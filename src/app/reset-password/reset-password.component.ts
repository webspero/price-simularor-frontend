import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MustMatch } from '../register/_helpers/must-match.validator';
import { Location } from '@angular/common';
import { ApiServiceService } from '../api-service.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
	message;
	CurrentUser;
	user_email;
	resetpass_form:FormGroup;
	NewPassword;
	ConfirmPassword;
	successClass;
	recovery_key;
	passwordchnageform;
	CurrentPassword;
	submitted = false;
	constructor(
		private formBuilder: FormBuilder,
		private router: Router,
		private apiService: ApiServiceService,
		private _location: Location
	) {

		this.resetpass_form = this.formBuilder.group({
			NewPassword: ['', [Validators.required, Validators.minLength(8),Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')]],
			ConfirmPassword: ['', [Validators.required, Validators.minLength(8),Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')]]
		}, {
			validator: MustMatch('NewPassword', 'ConfirmPassword')
		})
	}
   
	ngOnInit() {
		var top_url = this.router['routerState']['snapshot']['url'].split("/reset-password/");
		this.recovery_key = top_url[1];
	}
	
	get f() {
		return this.resetpass_form.controls;
	}

	showPassword(event,CurrentPassword) {
		let target = event.target || event.srcElement || event.currentTarget;
		if(event.target.previousSibling.attributes.type.value == "password"){
			event.target.previousSibling.setAttribute("type","text");
			event.target.className = "fa fa-eye";
		}else{
			event.target.previousSibling.setAttribute("type","password");
			event.target.className = "fa fa-eye-slash";
		}
	}

	login() {
		this.router.navigate(['login']);
	}

	register() {
		this.router.navigate(['register']);
	}

	ResetPassword(vals){
		this.submitted = true;
		var values = {
			"recovery_key": this.recovery_key,
			"new_password":this.resetpass_form.value.NewPassword
		};
		this.apiService.Reset_Password(values).subscribe((response:any)=>{
			if(response.success == false && response.message == "Token is not valid") {
			} else if(response.status == 200) {
				this.resetpass_form.reset();
				this.message = response.response + ' Please login again!!';
				this.successClass = 'alert-success';
				setTimeout (() => {
					this.router.navigate(['login']);
				}, 3000);
			}else{
				this.message = response.error.error;
				this.successClass = 'alert-danger';
			}
		},(err)=>{
			this.message = err.error.error;
			this.successClass = 'alert-danger';
		});

	}
}