import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import * as $ from 'jquery';
import { environment } from '../../environments/environment';
import { ApiServiceService } from '../api-service.service';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
	searchForm;
	name;
	token
	profileimg:string = ""
	public userDetail : any 
	public checkValue : boolean = false
	constructor(
		private router: Router,
		private _location: Location,
		private apiService:ApiServiceService
	) {
		this.token = localStorage.getItem("token");
		this.userDetail = JSON.parse(localStorage.getItem('userdetail'))
		this.apiService.currentUser(this.userDetail.token,this.userDetail.api_key).subscribe((response:any)=>{
			if (response.success == false && response.message == "Token is not valid") {
				localStorage.clear();
				this.router.navigate(['login']);
			}
		},(err)=>{
			localStorage.clear();
			this.router.navigate(['login']);
		});
		
		this.name = localStorage.getItem("username");
	}

	ngOnInit() {
		this.get_CurrentUser()
		if ($(window).width() < 992) {
			$("#removetoggled").removeClass('toggled');
		}
		this.searchForm = new FormGroup({
			search: new FormControl(''),
		});
		$(".sidebar-dropdown > a").click(function () {
			$(".sidebar-submenu").slideUp(200);
			if ($(this).parent().hasClass("active")) {
				$(".sidebar-dropdown").removeClass("active");
			} else {
				$(".sidebar-dropdown").removeClass("active");
				$(this).next(".sidebar-submenu").slideDown(200);
				$(this).parent().addClass("active");
			}
		});
		$("#close-sidebar").click(function () {
			$(".boxed-container .container").css("max-width","100%");
			$(".boxed-container").css("max-width","100%");
			$(".boxed-container").css("width","100%");
			$(".boxed-container").css("float","none");
		});


		$("#show-sidebar").click(function () {
			$(".boxed-container .container").css("max-width","");
			$(".boxed-container").css("width","");
			$(".boxed-container").css("max-width","");
			$(".boxed-container").css("float","");
		});


		$("#close-sidebar").click(function () {
			$(".page-wrapper").removeClass("toggled");
		});
		$("#show-sidebar").click(function () {
			$(".page-wrapper").addClass("toggled");
		});
	}

	backClicked() {
		this._location.back();
	}

	onSidebarClose() {
		$("#header").css("width", "100%");
	}

	onSidebaropen() {
		var width = $("#header").width() - 220;
		$("#header").css("width", width);
		$("#show-sidebar").click(function () {
			$("#header").css("transition-duration", ".6s");
		});
	}

	logout() {
		localStorage.clear();
		this.router.navigate['/'];
	}

	get_CurrentUser(){
		this.apiService.currentUser(this.userDetail.token,this.userDetail.api_key).subscribe((response:any)=>{
			if(response && response.length > 0 && response[0].profile_image){
				this.profileimg = environment.backendUrl+''+response[0].profile_image;
			}else{
				this.profileimg = environment.backendUrl+'uploads/assets/img/default.png';
			}
		},(err)=>{
			console.log(err);
		});  
	}

	ngDoCheck(){
		var that = this
		$(window).click(function(e) {
			that.checkValue=false
		});
	}
}


