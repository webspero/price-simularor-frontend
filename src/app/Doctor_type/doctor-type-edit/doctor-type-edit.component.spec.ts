import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorTypeEditComponent } from './doctor-type-edit.component';

describe('DoctorTypeEditComponent', () => {
  let component: DoctorTypeEditComponent;
  let fixture: ComponentFixture<DoctorTypeEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorTypeEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorTypeEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
