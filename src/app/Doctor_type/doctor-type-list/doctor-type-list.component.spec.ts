import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorTypeListComponent } from './doctor-type-list.component';

describe('DoctorTypeListComponent', () => {
  let component: DoctorTypeListComponent;
  let fixture: ComponentFixture<DoctorTypeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorTypeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorTypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
