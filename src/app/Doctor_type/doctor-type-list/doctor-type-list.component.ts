import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../../api-service.service';
import Swal from 'sweetalert2'
import {FormGroup, FormBuilder, Validators} from '@angular/forms'
import * as moment from 'moment';

@Component({
  selector: 'app-doctor-type-list',
  templateUrl: './doctor-type-list.component.html',
  styleUrls: ['./doctor-type-list.component.css']
})
export class DoctorTypeListComponent implements OnInit {

  public userDetail : any 
  public doctorList :any
  public range : number = 10
  public activePage :number = 1
  public apiRefresh:Boolean = false
  public totalCount:number
  public modelId : any
  public doctorForm : FormGroup
  public isSubmit : boolean = false
  public submitted:boolean=false
  public popupSub:boolean=false
  constructor(private apiService:ApiServiceService,private formbuilder : FormBuilder) { }

  ngOnInit() {
    this.userDetail = JSON.parse(localStorage.getItem('userdetail'))
    this.getDoctorList() 
    this.doctorForm = this.formbuilder.group({
      doc_type:['',[Validators.required]],
      status:[1,Validators.required]
    })
  }
  ngDoCheck(){
		if(this.apiRefresh){
			this.getDoctorList()
			this.apiRefresh=false
		}
  }
  
  pageChanged(data){
    this.activePage=data
    this.getDoctorList()
  }
  getDoctorList(){
		this.apiService.getallDoctorType(this.userDetail.token,this.activePage.toString(),this.range.toString()).subscribe((data:any)=>{
      this.doctorList= data.results
      this.totalCount = data.totalCount
		},(error)=>{
			console.log(error)
		},()=>{
    })
  }
  
   deleteProcedure(id){
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to delete this record!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.apiService.deleteDoctorTypeById(this.userDetail.token,id).subscribe((data)=>{
          this.getDoctorList() 
          Swal.fire(
            'Deleted!',
            'Your record has been deleted.',
            'success'
          )
        },(error)=>{

        },()=>{

        })
      // For more information about handling dismissals please visit
      // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'Your record is safe :)',
          'error'
        )
      }
    })
  }

  editDoctype(id){
    this.modelId = id
    this.apiService.getDoctorTypeById(this.userDetail.token,id).subscribe((data : any)=>{
      this.doctorForm.patchValue({
        doc_type:data && data.results && data.results.doc_type
      })
    },(error)=>{
      console.log(error)
    })
  }

  addDoctype(){
    this.doctorForm
    this.modelId="";
    this.popupSub=false;
    this.doctorForm.controls['doc_type'].setValue('');
  }
  
  ApiRefresh(refresh){
		this.apiRefresh=refresh
	}

  displayActivePage(activePageNumber:number){
		this.activePage = activePageNumber;
  }  
  
  submit(){
    if(this.submitted) return;
    this.popupSub=true;
    this.isSubmit= true
    this.submitted = true
    if(this.doctorForm.invalid) return this.submitted = false
    
    if(this.modelId){
      // edit
      this.apiService.updateDoctorType(this.userDetail.token,this.modelId,this.doctorForm.value).subscribe((data)=>{
        this.isSubmit= false
        this.submitted = false
        this.getDoctorList()
        document.getElementById('close').click()
      },(error)=>{
        this.isSubmit= false
        this.submitted = false
        console.log(error)
      },()=>{
        this.isSubmit= false
        this.submitted = false
      })
    }else{
      // add
      this.apiService.createDoctorType(this.userDetail.token,this.doctorForm.value).subscribe((data)=>{
        this.isSubmit=false
        this.submitted = false
        this.getDoctorList()
        document.getElementById('close').click()
      },(error)=>{
        console.log(error)
        this.isSubmit=false
        this.submitted = false
      },()=>{
        this.isSubmit=false
        this.submitted = false
      })
    }
  }

date_change(date){
  const formatedDate = moment(date).format('MM/DD/YYYY')
  return formatedDate
}
}
