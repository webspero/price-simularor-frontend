import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiServiceService } from '../api-service.service';

@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.component.html',
  styleUrls: ['./forgetpassword.component.css']
})
export class ForgetpasswordComponent implements OnInit {
	forgotForm;
  //token;
  user_email;
  submitted = false;
  message;
  error;
  successClass;
  constructor(
    private router: Router,
    private apiService: ApiServiceService,
    private formBuilder: FormBuilder,
  	)
  { 
    // this.token = localStorage.getItem("token");
    // if (this.token) {
    //   this.router.navigate(['dashboard']);
    // }
  }

  ngOnInit() {
  	this.forgotForm = this.formBuilder.group({
      user_email: ['', [Validators.required, Validators.email, Validators.pattern(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)]],
    });
  }

  get f() {
    return this.forgotForm.controls;
  }
 
  onSubmit(val){
      this.submitted = true;
      this.user_email = val.user_email;
      this.forgotForm.reset();
        this.apiService.forgetPassword(this.user_email).subscribe((response: any) => {
          
          if(response.status == 200){
              this.successClass = 'alert-success';
              this.message = response.response;
              
          }else{
            this.message = "Something went wrong!";
          }
        }, (err) => {
           this.message= err.error.error;
           this.successClass = 'alert-danger';
        })
    }
  }