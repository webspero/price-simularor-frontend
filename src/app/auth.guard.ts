import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree,Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {
	constructor(private router: Router) { }

	canActivate():boolean {
		const currentUser = localStorage.getItem("token");
		if (currentUser) {
			return true;
		}
		else {
			localStorage.clear();
			this.router.navigate(['/']);
			return false;
		}
	}
}