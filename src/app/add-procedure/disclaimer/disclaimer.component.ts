import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiServiceService } from '../../api-service.service';
import { TokenexpiryService } from '../../tokenexpiry.service';
import { Location } from '@angular/common';
import Swal from 'sweetalert2'

@Component({
	selector: 'app-disclaimer',
	templateUrl: './disclaimer.component.html',
	styleUrls: ['./disclaimer.component.css']
})

export class DisclaimerComponent implements OnInit {
	public apikey;
	public token;
	public disclaimerForm; 
	public message;
	
	constructor(
		private activatedRoute: ActivatedRoute,
		private http: HttpClient,
		private router: Router,
		private apiService: ApiServiceService,
		private expireTokenService: TokenexpiryService,
		private _location: Location,
		public fb: FormBuilder
	) {
		this.token = localStorage.getItem("token");
		this.apikey = localStorage.getItem("apikey");
		this.disclaimerForm = this.fb.group({
			disclaimer: ['', [Validators.required]],
		});
		this.userData();
	}

	ngOnInit() {
		
	}

	userData(){
		this.apiService.currentUser(this.token,this.apikey).subscribe((response:any)=>{
			if(response.length > 0){
				if(response[0].disclaimer !== ''){
					this.disclaimerForm.patchValue({
						disclaimer:response[0].disclaimer
					})
				}else{
					this.disclaimerForm.patchValue({
						disclaimer:'Prices are estimated cash/self-pay prices outside of insurance. Prices are estimates only and prices may range according to the individual.'
					})
				}
			}else{
				this.disclaimerForm.patchValue({
					disclaimer:'Prices are estimated cash/self-pay prices outside of insurance. Prices are estimates only and prices may range according to the individual.'
				})
			}
		},(err)=>{
			console.log(err);
		});
	}

	logValue(val){
		this.apiService.uploadUsermeta(this.token,this.apikey,val).subscribe((res: any) => {
			if (res.status === 200) {
				this.disclaimerForm.controls['disclaimer'].setValue(val.disclaimer, {onlySelf: true});
				// this.message = "Disclaimer Updated!!";
				Swal.fire(
					'Success',
					'Disclaimer successfully updated',
					'success'
				)
			} else {
				this.message = 'Something Went Wrong';
			}
		},(err) => {
			console.log(err);
		})
	}

	backClicked(){
		this._location.back();
	}

}
