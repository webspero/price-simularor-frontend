import { Component, OnInit } from '@angular/core';
import { MultiSelectAllModule } from '@syncfusion/ej2-angular-dropdowns';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiServiceService } from '../api-service.service';
import { TokenexpiryService } from '../tokenexpiry.service';
import * as $ from 'jquery';
import { Location } from '@angular/common';
import Swal from 'sweetalert2'


@Component({
	selector: 'app-add-procedure',
	templateUrl: './add-procedure.component.html',
	styleUrls: ['./add-procedure.component.css']
})
export class AddProcedureComponent implements OnInit {
	
	public surgeryType: any = [];
	public procedureArray: any = [];
	public moduleList: any = [];
	public genderCheck: any = [];
	public area_interest: any = [];
	public errormsg;
	public submitted;
	public apikey;
	public token;
	public shhowGender:boolean = false;
	public procdeureForm: FormGroup;
	public message
	public doctype;
	public module_type: String;
	public description;
	public interestType;
	public surgicalType = '';
	public genderVal = "Both";
	public userDetail:any
	public gender =[
		{name:"Male"},
		{name:"Female"},
		{name:"Both"}
	]
	public issubmitted:boolean=false

	constructor(
		private activatedRoute: ActivatedRoute,
		private http: HttpClient,
		private router: Router,
		private apiService: ApiServiceService,
		private expireTokenService: TokenexpiryService,
		private _location: Location,
		public fb: FormBuilder
	) {
		this.userDetail = JSON.parse(localStorage.getItem('userdetail')) 
		this.token = localStorage.getItem("token");
		this.apikey = localStorage.getItem("apikey");
	}

	ngOnInit() {
		this.genderCheck.lenght = 1;
		this.procdeureForm = this.fb.group({
			module_type: ['', Validators.required],
			area_interest_val: ['', Validators.required],
			procedure_gender: ['', Validators.required],
			procedure_type: [null],
			diseaseType: [''],
			procedure_name:['', Validators.required],
			prod_desc: ['', Validators.required],
			procedure:['newProcedure'],
			//procedure_disclaimer: ['Price are estimated cash / self-pay prices outsite of insurance.', Validators.required],
			procedure_cost_female: ['0', [Validators.required,Validators.pattern("^[0-9-]*$")]],
			procedure_cost_male: ['0', [Validators.required,Validators.pattern("^[0-9-]*$")]],
			procedure_cost_start: ['0', [Validators.pattern("^[0-9-]*$")]],
		});
		this.loadModule();
	}

	get f() {
		return this.procdeureForm.controls;
	}

	backClicked() {
		this._location.back();
	}

	loadModule() {
		this.apiService.currentUser(this.token, this.apikey).subscribe((response: any) => {
			this.message = "";
			if (response.success == false && response.message == "Token is not valid") {
				this.expireTokenService.expireToken();
			} else if (response.length > 0) {
				this.doctype = response[0].doctype;
				this.Get_doctype();
			} else {
				this.message = "Something went wrong loadModule!";
			}
		}, (err) => {
			console.log(err);
		});
	}

	Get_doctype() {
		this.message = "";
		var id = this.doctype.toString();
		this.apiService.GetDoctypesById(id).subscribe((response: any) => {
			if (response.success == false && response.message == "Token is not valid") {
				this.expireTokenService.expireToken();
			} else if (response.response.length > 0) {
				this.areaInterest(response.response[0].id)
				this.moduleList = response.response;
				this.procdeureForm.patchValue({
					module_type:response.response[0].id
				})
				
				//this.procdeureForm.controls['module_type'].setValue(this.moduleList[0].module_name, {onlySelf: true});
				this.module_type = this.moduleList[0].id;
			} else {
				this.message = "Something went wrong Get_doctype!";
			}
		}, (err) => {
			console.log(err);
		});
	}

	onChangeModule(value) {
		if (value === null) {
			value = '';
		}
		this.procedureArray = [];
		this.description = '';
		this.procdeureForm.controls['diseaseType'].setValue(null);
		this.procdeureForm.controls['procedure_name'].setValue('');
		this.module_type = value.toString();
		this.areaInterest(value);
	}

	areaInterest(value) {
		this.message = "";
		this.apiService.getModule(this.token, this.apikey, value.toString()).subscribe((response: any) => {
			if (response !== null && response.length > 0) {
				this.area_interest = response;
			} else {
				this.area_interest = [];
				this.message = "Please Select Module Type!";
			}
			this.callProcedure(this.module_type, this.interestType, this.genderVal, this.surgicalType);
		}, (err) => {
			console.log(err);
		});
	}

	getGender(val) {
		if (val === null) {
			val = '';
		}
		this.interestType = val;
		this.procdeureForm.controls['diseaseType'].setValue(null);
		this.procedureArray = [];
		this.description = '';
		this.apiService.getGender(this.token, this.apikey, this.module_type, val).subscribe((response: any) => {
			if (response !== null && response.status == 200) {
				if (response.response.length > 1) {
					this.genderCheck = response.response;
				} else if (response.response[0].gender !== "Both") {
					this.genderVal = response.response[0].gender;
				} else {
					this.genderCheck = 1
				}
				this.callProcedure(this.module_type, this.interestType, this.genderVal, this.surgicalType);
			} else {
				this.message = "Please Select Area of Interest!";
			}
		}, (err) => {
			// console.log(err);
		})
	}

	checkGender(val) {
		if (val === null) {
			val = '';
		}
		this.procdeureForm.controls['diseaseType'].setValue(null);
		this.procedureArray = [];
		this.description = '';
		this.genderVal = val;
		this.callProcedure(this.module_type, this.interestType, this.genderVal, this.surgicalType);
	}

	callProcedure(module, interstType, gender, procedure) {
		this.apiService.getProcedure(this.token, this.apikey, module.toString(), interstType, gender, procedure).subscribe((response: any) => {
			if (response && response.length > 0) {
				this.procedureArray = response;
				var flags = [],
					output = [],
					l = this.procedureArray.length,
					i;
				for (i = 0; i < l; i++) {
					if (flags[this.procedureArray[i].type]) continue;
					flags[this.procedureArray[i].type] = true;
					if (this.procedureArray[i].type) {
						output.push(this.procedureArray[i].type);
					}
				}
				if (output.length > 0) {
					this.surgeryType = output;
				}
			} else {
				this.procedureArray = [];
				this.procdeureForm.controls['diseaseType'].setValue(null);
				this.description = '';
				this.surgeryType = "";
			}
		}, (err) => {
			// console.log(err);
		})
	}

	diseaseForm(valcehck) {
		if (valcehck === null) {
			this.description = '';
		} else {
			for (var k = 0; k < this.procedureArray.length; k++) {
				if (this.procedureArray[k].id == valcehck) {
					this.description = this.procdeureForm.controls['prod_desc'].setValue(this.procedureArray[k].description);
				}
			}
		}
	}

	onGenderChange(value){
		this.genderVal=value
		this.callProcedure(this.module_type, this.interestType, this.genderVal, this.surgicalType);
	}

	onChangesurgicalType(val) {
		if (val === null) {
			val = '';
		}
		this.procedureArray = [];
		this.procdeureForm.controls['diseaseType'].setValue(null);
		this.description = '';
		this.surgicalType = val;
		this.callProcedure(this.module_type, this.interestType, this.genderVal, this.surgicalType);
	}

	onChange(value){
		this.procdeureForm.patchValue({
			procedure:value
		})
	}

	//Submit Form
	logValue(formVal) {
	console.log(this.issubmitted)
	console.log(formVal)
	console.log(this.procdeureForm)
		this.issubmitted=true
		if(!this.procdeureForm.valid) return
		
		/*if(this.procdeureForm.value.procedure==='newProcedure' && this.procdeureForm.value.procedure_name===''){
			this.errormsg = "Procedure Name is missing"
			this.issubmitted=false
			return;
		}

		if(this.procdeureForm.value.procedure==='registerProcedure' && this.procdeureForm.value.diseaseType===null){
			this.errormsg = "Please Select Procedure Name"
			this.issubmitted=false
			return;
		}
		this.submitted = true;
		if(this.procdeureForm.invalid) return this.issubmitted=false
		if(this.procdeureForm.value.procedure === "newProcedure" && this.procdeureForm.value.procedure_gender === "" && this.procdeureForm.value.procedure_name === ""){
			this.message = "Gender and Procedure name required"
			this.issubmitted=false
			return;
		}
		*/
		var datasets = {};

		
		const body = {
			admin_id:this.userDetail.id,
			module_type:this.procdeureForm.value.module_type,
			area_interest:this.procdeureForm.value.area_interest_val,
			prod_gender:this.procdeureForm.value.procedure_gender==="" ? this.genderVal : this.procdeureForm.value.procedure_gender,
			type:this.procdeureForm.value.procedure_type,
			procedure_id:this.procdeureForm.value.diseaseType,
			procedure_name:this.procdeureForm.value.procedure_name,
			prod_desc:this.procdeureForm.value.prod_desc,
			male_cost:this.procdeureForm.value.procedure_cost_male,
			female_cost:this.procdeureForm.value.procedure_cost_female,
		}

		this.apiService.addProcedureadmin(body, this.token, this.apikey).subscribe((response: any) => {
			console.log(response)
			if (response.success == false && response.message == "Token is not valid") {
				this.expireTokenService.expireToken();
			} else if (response.status == 200) {
				Swal.fire(
					'Success!',
					"Procedure added successfully!",
					'success'
				  )
				this.message = "Procedure added successfully!";
				this.router.navigate(['view']);
				this.issubmitted=false
			} else {
				this.message = "Something went wrong!";
				this.issubmitted=false
			}
		}, (err) => {
			console.log(err);
		})
		
	}
}