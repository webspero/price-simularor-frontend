import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler, Injectable } from '@angular/core';
import { HashLocationStrategy, PathLocationStrategy, LocationStrategy } from '@angular/common';
// import * as Sentry from '@sentry/browser';
// import { RewriteFrames } from '@sentry/integrations';
import { AuthGuard } from './auth.guard';
import { FormDirective } from './form.directive';

//Filter Pagination
import { NgxPaginationModule } from 'ngx-pagination';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { SelectDropDownModule } from 'ngx-select-dropdown';
import { NgxCurrencyModule } from "ngx-currency";
import { NgPipesModule } from 'ngx-pipes';

//Auth
import { ChartsModule } from 'ng2-charts';
import { AuthService } from './auth/auth.service';
import { RoleGuardService } from './role-guard.service';
import { DataTablesModule } from 'angular-datatables';
import { MyDatePickerModule } from 'mydatepicker';
//import { JwPaginationComponent } from 'jw-angular-pagination';
//import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
/*****External*****/
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
//import { HeaderModuleModule } from './header-module/header-module.module';

import { ProcedureSearchPipe } from './VendorProcedure/vendor-procedure-list/pipesearch';
import { LoginComponent } from './login/login.component';
import { ForgetpasswordComponent } from './forgetpassword/forgetpassword.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserCreateComponent } from './users/user-create/user-create.component';
import { UserViewComponent } from './user/user-view/user-view.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AddUserComponent } from './add-user/add-user.component';
import { AddProcedureComponent } from './add-procedure/add-procedure.component';
import { AddViewComponent } from './add-view/add-view.component';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { EditProcedureComponent } from './edit-procedure/edit-procedure.component';
import { UserEditComponent } from './user/user-edit/user-edit.component';
import { SettingsComponent } from './settings/settings.component';
import { ManageProfileComponent } from './manage-profile/manage-profile.component';
import { ChangePasswordComponent } from './user/change-password/change-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { PaginationComponent } from './pagination/pagination.component'
import { ViewComponent } from './webuser/view/view.component';
import { EditComponent } from './webuser/edit/edit.component';
import { ListComponent } from './webuser/list/list.component';
import { ProcedureListComponent } from './Procedure/procedure-list/procedure-list.component';
import { ProcedureEditComponent } from './Procedure/procedure-edit/procedure-edit.component';
import { DoctorTypeListComponent } from './Doctor_type/doctor-type-list/doctor-type-list.component';
import { DoctorTypeEditComponent } from './Doctor_type/doctor-type-edit/doctor-type-edit.component';
import { ModulelistComponent } from './Module_data/modulelist/modulelist.component';
import { ProcedurecategorylistComponent } from './Procedure category/procedurecategorylist/procedurecategorylist.component';
import { ProcedureAddComponent } from './Procedure/procedure-add/procedure-add.component';
import { VendorProcedureListComponent } from './VendorProcedure/vendor-procedure-list/vendor-procedure-list.component';
import { UserdetailsComponent } from './userdetails/userdetails.component';
import { DisclaimerComponent } from './add-procedure/disclaimer/disclaimer.component';
import { VendorprocedureeditComponent } from './VendorProcedure/vendorprocedureedit/vendorprocedureedit.component';
import { AdminProcedureViewComponent } from './admin-procedure-view/admin-procedure-view.component';

// Sentry.init({
// 	dsn: 'https://aaf9676d6abd43fc8a585372e0b84dc9@o373929.ingest.sentry.io/5191201',
// 	integrations: [
// 		new RewriteFrames(),
// 	],
// });

// @Injectable()
// export class SentryErrorHandler implements ErrorHandler {
// 	constructor() {}
// 	handleError(error) {
// 		const eventId = Sentry.captureException(error.originalError || error);
// 		Sentry.showReportDialog({
// 			eventId
// 		});
// 	}
// }

export const customCurrencyMaskConfig = {
	align: "left",
	allowNegative: true,
	allowZero: true,
	decimal: ",",
	precision: 0,
	prefix: "USD $",
	suffix: "",
	thousands: ".",
	nullable: true
};

@NgModule({
	declarations: [
		FormDirective,
		AppComponent,
		LoginComponent,
		ForgetpasswordComponent,
		RegisterComponent,
		HomeComponent,
		HeaderComponent,
		DashboardComponent,
		UserCreateComponent,
		UserViewComponent,
		AddUserComponent,
		AddProcedureComponent,
		AddViewComponent,
		EditProcedureComponent,
		UserEditComponent,
		SettingsComponent,
		ManageProfileComponent,
		ChangePasswordComponent,
		ResetPasswordComponent,
		PaginationComponent,
		ViewComponent,
		EditComponent,
		ListComponent,
		ProcedureListComponent,
		ProcedureEditComponent,
		DoctorTypeListComponent,
		DoctorTypeEditComponent,
		ModulelistComponent,
		ProcedurecategorylistComponent,
		ProcedureAddComponent,
		VendorProcedureListComponent,
		UserdetailsComponent,
		DisclaimerComponent,
		VendorprocedureeditComponent,
		AdminProcedureViewComponent,
		ProcedureSearchPipe,
	],
	imports: [
		MyDatePickerModule,
		NgxPaginationModule,
		BrowserModule,
		DataTablesModule,
		AppRoutingModule,
		HttpClientModule,
		BrowserAnimationsModule,
		PaginationModule.forRoot(),
		NgxCurrencyModule.forRoot(customCurrencyMaskConfig),
		NgPipesModule,
		AngularFontAwesomeModule,
		SelectDropDownModule,
		ChartsModule,
		ModalModule.forRoot(),
		CollapseModule.forRoot(),
		FormsModule,
		ReactiveFormsModule
	],
	providers: [AuthService, RoleGuardService, {provide: LocationStrategy,useClass: PathLocationStrategy}, AuthGuard],
	bootstrap: [AppComponent]
})
export class AppModule {}