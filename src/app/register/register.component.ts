import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiServiceService } from  '../api-service.service';
import { MustMatch } from './_helpers/must-match.validator';
import Swal from 'sweetalert2'

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
	regForm:FormGroup;
	message;
	token;
	submitted = false;
	doctypes;
	States = [
		{'id':'1', 'value': 'Alabama'},
		{'id':'2', 'value': 'Alaska'},
		{'id':'3', 'value': 'Arizona'},
		{'id':'4', 'value': 'Arkansas'},
		{'id':'5', 'value': 'California'},
		{'id':'6', 'value': 'Colorado'},
		{'id':'7', 'value': 'Connecticut'},
		{'id':'8', 'value': 'Delaware'},
		{'id':'9', 'value': 'District of Columbia'},
		{'id':'10', 'value': 'Florida'},
		{'id':'11', 'value': 'Georgia'},
		{'id':'12', 'value': 'Hawaii'},
		{'id':'13', 'value': 'Idaho'},
		{'id':'14', 'value': 'Illinois'},
		{'id':'15', 'value': 'Indiana'},
		{'id':'16', 'value': 'Iowa'},
		{'id':'17', 'value': 'Kansas'},
		{'id':'18', 'value': 'Kentucky'},
		{'id':'19', 'value': 'Louisiana'},
		{'id':'20', 'value': 'Maine'},
		{'id':'21', 'value': 'Maryland'},
		{'id':'22', 'value': 'Massachusetts'},
		{'id':'23', 'value': 'Michigan'},
		{'id':'24', 'value': 'Minnesota'},
		{'id':'25', 'value': 'Mississippi'},
		{'id':'26', 'value': 'Missouri'},
		{'id':'27', 'value': 'Montana'},
		{'id':'28', 'value': 'Nebraska'},
		{'id':'29', 'value': 'Nevada'},
		{'id':'30', 'value': 'New Hampshire'},
		{'id':'31', 'value': 'New Jersey'},
		{'id':'32', 'value': 'New Mexico'},
		{'id':'33', 'value': 'New York'},
		{'id':'34', 'value': 'North Carolina'},
		{'id':'35', 'value': 'North Dakota'},
		{'id':'36', 'value': 'Ohio'},
		{'id':'37', 'value': 'Oklahoma'},
		{'id':'38', 'value': 'Oregon'},
		{'id':'39', 'value': 'Pennsylvania'},
		{'id':'40', 'value': 'Rhode Island'},
		{'id':'41', 'value': 'South Carolina'},
		{'id':'42', 'value': 'South Dakota'},
		{'id':'43', 'value': 'Tennessee'},
		{'id':'44', 'value': 'Texas'},
		{'id':'45', 'value': 'Utah'},
		{'id':'46', 'value': 'Vermont'},
		{'id':'47', 'value': 'Virginia'},
		{'id':'48', 'value': 'Washington'},
		{'id':'49', 'value': 'West Virginia'},
		{'id':'50', 'value': 'Wisconsin'},
		{'id':'51', 'value': 'Wyoming'}
	];

	issubmit:boolean=false

	constructor(
		private router: Router,
		private formBuilder: FormBuilder,
		private activatedRoute: ActivatedRoute,
		private apiService:  ApiServiceService,
		private http: HttpClient
		//public auth: AuthService,
	)
	{
		this.token = localStorage.getItem("token");
		if (this.token) {
			this.router.navigate(['dashboard']);
		}
		this.regForm = this.formBuilder.group({
			doctortype: ['', Validators.required],
			FirstName: ['', [this.noWhitespaceValidator, Validators.minLength(1), Validators.required, Validators.pattern(/^[\w -]*$/)]],
			LastName: ['', [this.noWhitespaceValidator, Validators.minLength(1), Validators.required, Validators.pattern(/^[\w -]*$/)]],
			name_of_point:['', [this.noWhitespaceValidator, Validators.minLength(3), Validators.pattern(/^[\w -]*$/)]],
			name_of_point_admin:['', [this.noWhitespaceValidator, Validators.minLength(3), Validators.pattern(/^[\w -]*$/)]],
			address: ['', [this.noWhitespaceValidator, Validators.minLength(3), Validators.required, Validators.pattern(/^[\w -]*$/)]],
			city: ['', [this.noWhitespaceValidator, Validators.minLength(3), Validators.required,Validators.pattern(/^[\w -]*$/)]],
			state: ['', [Validators.required]],
			email_of_point: ['', [Validators.required, Validators.email, Validators.pattern(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)]],
			email_of_point_admin: ['', [Validators.required, Validators.email, Validators.pattern(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)]],
			provider_business_email: ['', [Validators.required, Validators.email, Validators.pattern(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)]],
			providerurl:['', [this.noWhitespaceValidator, Validators.minLength(3), Validators.required]],
			Password: ['', [Validators.required,this.noWhitespaceValidator.bind(this), Validators.minLength(8),Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')]],
			ConfirmPassword: ['', [Validators.required,this.noWhitespaceValidator.bind(this), Validators.minLength(8)]],
			MobileNo: ['', [ Validators.required,Validators.minLength(8), Validators.maxLength(15),Validators.pattern(/^[0-9]*$/),this.noWhitespaceValidator.bind(this)]],
			country: ['', Validators.required],
			Zipcode: ['', [Validators.required, this.noWhitespaceValidator, Validators.minLength(3)]],
		}, {
			validator: MustMatch('Password', 'ConfirmPassword')
		})
	}

	get f() {
		return this.regForm.controls;
	}

	ngOnInit() {
		this.getTypes();
	}
  
	noWhitespaceValidator(control: FormControl) {
		const isWhitespace = (control.value || '').trim().length === 0;
		const isValid = !isWhitespace;
		return isValid ? null : { 'whitespace': true };
	}

	getTypes(){
		this.apiService.docTypes().subscribe((response:any) => {
			if(response['status'] === 200){
				this.doctypes = response['response'];
			}else{
				this.doctypes = [];
			}
		},(err) => {
			console.log(err);
		});
	}

	onSubmit(val){
		this.submitted = true;
		if(this.regForm.invalid) return
		if(this.issubmit) return
		this.issubmit = true
		this.apiService.signUp(val).subscribe((response:any) => {
			if(response['status']===200){
				this.regForm.reset()
				//this.message = response['response'];
				Swal.fire(
					'Success',
					response.response,
					'success'
				)
				this.issubmit = false
				setTimeout(() => {
					this.router.navigate(['/login']);
				}, 3000);
			}else{
				Swal.fire(
					'Cancelled',
					response.error,
					'error'
				)
				this.message = response['error'];
				this.issubmit = false
			}
		},(err) => {
			this.message = err['error'].error;
			Swal.fire(
				'Cancelled',
				err.error.error,
				'error'
			)
			console.log("error bloc",err);
			this.issubmit = false
		});
	}

	showPassword(event){
		let target = event.target || event.srcElement || event.currentTarget;
		if(event.target.previousSibling.attributes.type.value == "password"){
			event.target.previousSibling.setAttribute("type","text");
			event.target.className = "fa fa-eye";
		}else{
			event.target.previousSibling.setAttribute("type","password");
			event.target.className = "fa fa-eye-slash";
		}
	}

	ngOnDestroy(){
		this.regForm.reset();
	}
}
