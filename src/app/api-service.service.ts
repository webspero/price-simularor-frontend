import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from  '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable({
	providedIn: 'root'
})

export class ApiServiceService {
	API_URL  =  environment.backendUrl;
	//API_URL  =  "https://api.webspero.com:9090/"; //
	
	constructor( private  httpClient:  HttpClient ) {	}

	loginUser(data){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		return  this.httpClient.post(this.API_URL+'login',data, { headers: headers });
	}

	signUp(data){
		return  this.httpClient.post(this.API_URL+'register',data);
	}

	docTypes(){
		return  this.httpClient.get(this.API_URL+'doclist');
	}

	allModules(){
		return  this.httpClient.get(this.API_URL+'allmodule');
	}
	
	GetDoctypesById(id=null){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('id', id);
		return  this.httpClient.get(this.API_URL+'get_doctypes_by_id',{ headers: headers });
	}

	getleads(token,apikey){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		headers = headers.append('apikey', apikey);
		return  this.httpClient.get(this.API_URL+'getleads',{ headers: headers });
	}
	
	discount(token,apikey,data){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		headers = headers.append('apikey', apikey);
		return  this.httpClient.post(this.API_URL+'discount',data,{ headers: headers });
	}

	addProcedure(data,token,apikey){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		headers = headers.append('apikey', apikey);
		return  this.httpClient.post(this.API_URL+'addProcedure',data,{ headers: headers });
	}

	UpdateProcedure(data,token,apikey,prodid){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		headers = headers.append('apikey', apikey);
		return  this.httpClient.post(this.API_URL+'UpdateProcedure/'+prodid,data,{ headers: headers });
	}

	fileupload(token,apikey,data){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		headers = headers.append('apikey', apikey);
		return  this.httpClient.post(this.API_URL+'upload',data,{ headers: headers });
	}

	getModule(token,apikey,value){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		headers = headers.append('apikey', apikey);
		headers = headers.append('id', value);
		return  this.httpClient.get(this.API_URL+'getmodule',{ headers: headers });
	}
	
	getModuleId(token,apikey,value){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		headers = headers.append('apikey', apikey);
		headers = headers.append('id', value);
		return  this.httpClient.get(this.API_URL+'getmoduleid',{ headers: headers });
	}

	getGender(token,apikey,module_type,area_interest){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		headers = headers.append('apikey', apikey);
		headers = headers.append('module_type', module_type);
		headers = headers.append('area_interest', area_interest);
		return  this.httpClient.get(this.API_URL+'getgender',{ headers: headers });
	}

	updateProcedure(data,token,id,apikey){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		headers = headers.append('apikey', apikey);
		return  this.httpClient.post(this.API_URL+'updateProcedure/'+id,data,{ headers: headers });
	}

	deleteProcedure(id,token){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		return  this.httpClient.post(this.API_URL+'deleteProcedure',id,{ headers: headers });
	}

	getProcedurebyid(token,apikey,id){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		headers = headers.append('apikey', apikey);
		headers = headers.append('id', id);
		return  this.httpClient.get(this.API_URL+'probyid',{ headers: headers });
	}

	getProcedure(token,apikey,moduletype,interstType,gender,procedure){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		headers = headers.append('apikey', apikey);
		headers = headers.append('module_type', moduletype);
		headers = headers.append('area_interest', interstType);
		headers = headers.append('gender', gender);
		headers = headers.append('prcoedure_type', procedure);
		return  this.httpClient.get(this.API_URL+'getprocedure',{ headers: headers });
	}

	procedureByname(token,param1,param2){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		headers = headers.append('type', param1);
		headers = headers.append('body_part', param2);
		return  this.httpClient.get(this.API_URL+'procedure',{ headers: headers });
	}

	procedureBy(param,token,apikey){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		headers = headers.append('apikey', apikey);
		headers = headers.append('id', param);
		return  this.httpClient.get(this.API_URL+'procedurebyid',{ headers: headers });

	}

	getusersList(token,api){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		headers = headers.append('apikey', api);
		return  this.httpClient.get(this.API_URL+'getUser',{ headers: headers });
	}

	vendorRange(token,api,startdate,enddate,pageno,range){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		headers = headers.append('startdate', startdate);
		headers = headers.append('enddate', enddate);	
		return  this.httpClient.get(this.API_URL+'allvendor',{ headers: headers });
	}

	userRange(token,api,startdate,enddate,pageno,range,search,searchType){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		headers = headers.append('apikey', api);
		headers = headers.append('startdate', startdate);
		headers = headers.append('enddate', enddate);
		headers = headers.append('pageno', pageno);
		headers = headers.append('range', range);	
		headers = headers.append('search', search);
		headers = headers.append('searchType', searchType);	
		return  this.httpClient.get(this.API_URL+'range',{ headers: headers });
	}

	currentUser(token,api){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		headers = headers.append('apikey', api);
		return  this.httpClient.get(this.API_URL+'currentsuer',{ headers: headers });
	}

	uploadUsermeta(token,api,image){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		headers = headers.append('apikey', api);
		return  this.httpClient.post(this.API_URL+'usermeta',image,{ headers: headers });
	}
	
	UpdateUser(data=null,token=null,apikey=null){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		headers = headers.append('apikey', apikey);
		return  this.httpClient.post(this.API_URL+'update_user',data,{ headers: headers });
	}
	AddEmailTemp(data=null,token=null,apikey=null){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		headers = headers.append('apikey', apikey);
		return  this.httpClient.post(this.API_URL+'update_user',data,{ headers: headers });
	}
	forgetPassword(user_email=null){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('user_email', user_email);
		return  this.httpClient.get(this.API_URL+'forget_password',{ headers: headers });
	}
	ChangePassword(token=null,apikey=null,password_data=null){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		headers = headers.append('apikey', apikey);
		return  this.httpClient.post(this.API_URL+'change_password', password_data , { headers: headers });
	}
	Reset_Password(recovery_key=null){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		return  this.httpClient.post(this.API_URL+'reset_password', recovery_key , { headers: headers });
	}

	getuserbyId(token,api,userid){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		headers = headers.append('apikey', api);
		headers = headers.append('userid', userid);
		return  this.httpClient.get(this.API_URL+'userbyid',{ headers: headers });
	}

	getrequestbyuser(token){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('apikey', token);
		return  this.httpClient.get(this.API_URL+'viewqoute',{ headers: headers });
	}

	getProcedureList(token,apikey,pageno,range){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		headers = headers.append('apikey', apikey);
		headers = headers.append('pageno', pageno);
		headers = headers.append('range', range);
		return  this.httpClient.get(this.API_URL+'getProcedureList',{ headers: headers });
	}

	deleteUser(token,api){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		return  this.httpClient.post(this.API_URL+'deleteUser',api,{ headers: headers });
	}

	refreshToken(token){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		return  this.httpClient.post(this.API_URL+'auth/refresh',null,{ headers: headers });
	}

	expireToken(token){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		return  this.httpClient.post(this.API_URL+'auth/logout',null,{ headers: headers });
	}

	deviceList(token){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		return  this.httpClient.get(this.API_URL+'devices',{headers});
	}

	getWebuser(token,pageno,range,search,searchType){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer '+ token);
		headers = headers.append('pageno', pageno);
		headers = headers.append('range', range);
		headers = headers.append('search', search);
		headers = headers.append('searchType', searchType);
		return  this.httpClient.get(this.API_URL+'getwebuser',{headers});
	}

	getNewWebuser(token,pageno,range){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer '+ token);
		headers = headers.append('pageno', pageno);
		headers = headers.append('range', range);
		return  this.httpClient.get(this.API_URL+'getnewwebuser',{headers});
	}

	getWebuserbyid(token,apikey){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer '+ token);
		headers = headers.append('apikey', apikey);
		return  this.httpClient.get(this.API_URL+'getwebuserbyid',{headers});
	}

	deleteWebuserbyid(token,apikey){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer '+ token);
		headers = headers.append('apikey', apikey);
		return  this.httpClient.get(this.API_URL+'deletewebuserbyid',{headers});
	}

	deleteWebuser(token,apikey){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer '+ token);
		headers = headers.append('apikey', apikey);
		return  this.httpClient.get(this.API_URL+'DeleteWebUser',{headers});
	}

	getAllProcedureList(token,pageno,range){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		headers = headers.append('pageno', pageno);
		headers = headers.append('range', range);
		return  this.httpClient.get(this.API_URL+'getallprocedure',{ headers: headers });
	}

	createprocedure(token,body){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer '+ token);
		return  this.httpClient.post(this.API_URL+'createProcedure',body,{headers});
	}


	deleteProcedurebyid(token,id){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer '+ token);
		return  this.httpClient.post(this.API_URL+'deleteProcedurebyid',{id:id},{headers});
	}

	getallModule(token,pageno,range){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer '+ token);
		headers = headers.append('pageno', pageno);
		headers = headers.append('range', range);
		return  this.httpClient.get(this.API_URL+'getallmodule',{headers});
	}

	getModuleById(token,id){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer '+ token);
		return  this.httpClient.post(this.API_URL+'getmodulebyid',{id:id},{headers});
	}

	updatemodule(token,id,value){
		const body = Object.assign(value,{id:id})
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer '+ token);
		return  this.httpClient.post(this.API_URL+'updatemodulebyid',body,{headers});
	}

	createmodule(token,body){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer '+ token);
		return  this.httpClient.post(this.API_URL+'createmodule',body,{headers});
	}

	deleteModulebyid(token,id){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer '+ token);
		return  this.httpClient.post(this.API_URL+'deletemodulebyid',{id:id},{headers});
	}


	getallProcedureCategory(token,pageno,range){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer '+ token);
		headers = headers.append('pageno', pageno);
		headers = headers.append('range', range);
		return  this.httpClient.get(this.API_URL+'getallprocedurecategory',{headers});
	}

	getProcedureCategoryById(token,id){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer '+ token);
		return  this.httpClient.post(this.API_URL+'getprocedurecategorybyid',{id:id},{headers});
	}

	updateProcedureCategory(token,id,value){
		const body = Object.assign(value,{id:id})
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer '+ token);
		return  this.httpClient.post(this.API_URL+'updateprocedurecategorybyid',body,{headers});
	}

	createProcedureCategory(token,body){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer '+ token);
		return  this.httpClient.post(this.API_URL+'createprocedurecategory',body,{headers});
	}

	deleteProcedureCategoryById(token,id){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer '+ token);
		return  this.httpClient.post(this.API_URL+'deleteprocedurecategorybyid',{id:id},{headers});
	}

	getallDoctorType(token,pageno,range){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer '+ token);
		headers = headers.append('pageno', pageno);
		headers = headers.append('range', range);
		return  this.httpClient.get(this.API_URL+'getdoctype',{headers});
	}

	getDoctorTypeById(token,id){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer '+ token);
		return  this.httpClient.post(this.API_URL+'getdoctypebyid',{id:id},{headers});
	}

	updateDoctorType(token,id,value){
		const body = Object.assign(value,{id:id})
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer '+ token);
		return  this.httpClient.post(this.API_URL+'updatedoctypebyid',body,{headers});
	}

	createDoctorType(token,body){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer '+ token);
		return  this.httpClient.post(this.API_URL+'createdoctype',body,{headers});
	}

	deleteDoctorTypeById(token,id){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer '+ token);
		return  this.httpClient.post(this.API_URL+'deletedoctypebyid',{id:id},{headers});
	}

	getProcedureById(token,id){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer '+ token);
		return  this.httpClient.post(this.API_URL+'getProcedurebyid',{id:id},{headers});
	}

	updateProcedureById(token,value,id){
		var body = Object.assign(value,{"id":id})
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer '+ token);
		return  this.httpClient.post(this.API_URL+'updateProcedurebyid',body,{headers});
	}

	getVandorProcedureList(token,pageno,range){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		headers = headers.append('pageno', pageno);
		headers = headers.append('range', range);
		return  this.httpClient.get(this.API_URL+'getallvendorprocedure',{ headers: headers });
	}


	getAllProcedureListAdmin(token,pageno,range,admin_id,doctype){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		headers = headers.append('pageno', pageno);
		headers = headers.append('range', range);
		headers = headers.append('admin_id', admin_id);
		headers = headers.append('doctype', doctype);
		return  this.httpClient.get(this.API_URL+'getallprocedureadmin',{ headers: headers });
	}

	getProcedureByIdadmin(token,body){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer '+ token);
		return  this.httpClient.post(this.API_URL+'getProcedureByIdadmin',body,{headers});
	}

	getProcedureMetaByIdadmin(token,body){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer '+ token);
		return  this.httpClient.post(this.API_URL+'getProcedureMetaByIdadmin',body,{headers});
	}

	updateProcedureByIdadmin(body,token){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer '+ token);
		return  this.httpClient.post(this.API_URL+'updateProcedureByIdadmin',body,{headers:headers});
	}

	updateWebuserStatusbyid(token,apikey){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer '+ token);
		headers = headers.append('apikey', apikey);
		return  this.httpClient.get(this.API_URL+'updatestatus',{headers});
	}

	addProcedureadmin(data,token,apikey){
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append('Accept', 'application/json');
		headers = headers.append('Authorization', 'Bearer ' + token);
		headers = headers.append('apikey', apikey);
		return  this.httpClient.post(this.API_URL+'addProcedureadmin',data,{ headers: headers });
	}
}
