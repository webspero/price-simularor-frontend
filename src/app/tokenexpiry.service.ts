import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class TokenexpiryService {

  constructor(public router:Router) { }
  
  expireToken(){
    Swal.fire({
      title: 'Your session has expired',
      showCancelButton: false,
      allowOutsideClick: false
    }).then((value) => {
      if (value.value == true) {
        localStorage.clear();
        this.router.navigate(['login']);
      }
    });
  }
}
