import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MustMatch } from '../form-validation.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
    registerForm: FormGroup;
    submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private _location: Location
    ) { }

  ngOnInit() {
  	        this.registerForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(8)]],
            confirmPassword: ['', Validators.required],
            mobileNumber: ['', Validators.required]
        }, {
            validator: MustMatch('password', 'confirmPassword')
        });
  }
      // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }
backClicked() {
  this._location.back();
}
    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        // display form values on success
        alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4));
    }

}
