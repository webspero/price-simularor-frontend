import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../api-service.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-userdetails',
  templateUrl: './userdetails.component.html',
  styleUrls: ['./userdetails.component.css']
})
export class UserdetailsComponent implements OnInit {
	public apikey;
	public userkey;
	public message;
	public userQueried;
	public catList;
	public userForm;
	public resultsVal;
	public proceduReForm;
	public newLoop = [];
	myArray_1;
	myArray_2;
	totalValue;
	businessname = 'test';
	businessphone = '4545645465';
	disclaimer = "NA";
	public errorShow:boolean =false;

	constructor(
		private apiService: ApiServiceService,
		private activatedRoute: ActivatedRoute
	) { 
		this.userkey = this.activatedRoute.snapshot.paramMap.get('key');
	}

	ngOnInit() {
		this.dataShow();
	}

	dataShow(){
		this.apiService.getrequestbyuser(this.userkey).subscribe((response:any)=>{
			if(response.results.length > 0){
				this.userQueried = response.results[0];
				this.disclaimer = response.resultsVal[0].disclaimer;
				this.businessphone = response.resultsVal[0].mobile;
				this.businessname = response.resultsVal[0].name_of_point;
				this.userForm = response.results;
				this.resultsVal = response.resultsVal;
				var totalCost = [];
				var array_1_sum = [];
				var array_2_sum = [];
				this.catList = response.results.map( (value) => value.category_name).filter( (value, index, _arr) => _arr.indexOf(value) == index);
				if(this.resultsVal.length > 0){
					for(var nsk=0;nsk<this.resultsVal.length;nsk++){
						var productCost = this.resultsVal[nsk].prod_cost;
						var myexplode = productCost.split('-');
						if (myexplode.length < 2) {
							myexplode[1] = productCost;
						}
						array_1_sum.push(Number(myexplode[0]))
						array_2_sum.push(Number(myexplode[1]))
					}
					
					this.myArray_1 = Math.round(array_1_sum.reduce((a, b) => a + b, 0)).toFixed(2);
					this.myArray_2 = Math.round(array_2_sum.reduce((a, b) => a + b, 0)).toFixed(2);
					this.totalValue = Math.round(this.myArray_1) + '-' + Math.round(this.myArray_2);
					if (this.myArray_1 === this.myArray_2) {
						this.totalValue = Math.round(this.myArray_1);
					}
				}
				
			}else{
				this.errorShow = true;
				this.message = "No Procedure Found";
			}
		},(err)=>{
			console.log(err);
			this.errorShow = true;
			this.message = "Invalid Access";
		});
	}
}
