import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiServiceService } from '../api-service.service';
import { TokenexpiryService } from '../tokenexpiry.service';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { SingleDataSet, Color, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import { ExportToCsv } from 'export-to-csv';
import { IMyOptions, IMyDate, IMyDateModel } from 'mydatepicker';
import Swal from 'sweetalert2'
import * as moment from 'moment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {
	public d = new Date();
	public today;
	public monday;
	public StartDate;
	public EndDate;
	public api_startdate;
	public api_enddate;
	public token;
	public api_key;
	public userData= [];
	public leadData;
	public val;
	public procedurename;
	public countuser;
	public showexcel:boolean = true;
	public catArray = [];
	public prodArray = [];
	public mainarray = [];
	public dataVal = [];
	public downlaodExcel= [];
	public dataExcel = [];
	public activePage:number = 1;  
	public range:number = 10;
	public userDetail : any
	public webUser : any = []
	public newwebUser : any = []
	public vendorCount
	public apiRefresh:Boolean = false
	public totalCount : number =0
	public newTotalCount : number=0
	public endyear:any
	public endmonth:any
	public enddate:any

	public myDatePickerOptions: IMyOptions = {
		dateFormat: 'yyyy-mm-dd',
		showClearDateBtn: false,
		editableDateField: false,
		disableSince: {year: this.d.getFullYear(), month: this.d.getMonth() + 1, day: this.d.getDate() +1}
	};

	public options = { 
		fieldSeparator: ',',
		quoteStrings: '"',
		decimalSeparator: '.',
		showLabels: true, 
		showTitle: false,
		filename:'data_download',
		useTextFile: false,
		useBom: true,
		useKeysAsHeaders: true
	};
	
	public csvExporter = new ExportToCsv(this.options);

	public superadmin_user;
	public search : string = ''
	public searchType : string = 'email'

	constructor(
		private modalService: BsModalService,
		private formBuilder: FormBuilder,
		private apiService:ApiServiceService,
		private expireTokenService: TokenexpiryService,
	) {
		this.token = localStorage.getItem("token");
		this.api_key = localStorage.getItem("apikey");
		var _monday = new Date();
		var days = ((_monday.getDay() + 7) - 1) % 7;
		_monday.setDate(_monday.getDate() - days);
		this.monday = this.formatDate(_monday);
		this.StartDate = this.monday.format_1;
		var _today = new Date();
		this.today = this.formatDate(_today);
		this.EndDate = this.today.format_1;
		this.api_enddate = this.EndDate;
		this.userDetail = JSON.parse(localStorage.getItem('userdetail')) 
		if(this.userDetail.userType === 'superadmin'){
			this.getWebuser();
			this.getWebuserRange();
			this.getNewWebuser()
		}else{
			this.getUserList();
			this.rangeCount();
		}
		this.leadCount();
	}

	ngOnInit() {  }

	ngDoCheck(){
		if(this.apiRefresh){
			this.getWebuser()
			this.apiRefresh=false
		}
	}

	StartDateChanged(value){
		this.StartDate = value.formatted;
		var g1 = new Date(value.formatted); 
		var g2 = new Date(this.EndDate);
		if(g1.getTime() < g2.getTime()){
			this.api_startdate = value.formatted;
			if(this.userDetail.userType === 'superadmin'){
				this.getWebuserRange()
			}else{
				this.rangeCount();
			}
		}
		else{
			var startDate;
			var startDate_format = this.formatDate(g1);
			startDate = startDate_format.format_1;
			this.api_startdate = startDate;
			startDate = startDate.split("-");
			// this.StartDate = {'year':Number(startDate[0]),'month':Number(startDate[1]),'day': Number(startDate[2])};
			this.EndDate = {'year':Number(startDate[0]),'month':Number(startDate[1]),'day': Number(startDate[2])};
			alert("Please Select Less then date from End!");
			if(this.userDetail.userType === 'superadmin'){
				this.getWebuserRange()
			}else{
				this.rangeCount();
			}
		}
		
	}

	EndDateChanged(event:any){
		this.EndDate = event.formatted;
		var g1 = new Date(this.StartDate); 
		var g2 = new Date(event.formatted);
		if(g1.getTime() > g2.getTime()){
			var enddates;
			var end_dates = this.formatDate(g1);
			enddates = end_dates.format_1;
			this.api_enddate = enddates;
			enddates = enddates.split("-");
			this.EndDate = {'year':Number(enddates[0]),'month':Number(enddates[1]),'day': Number(enddates[2])}	
			// this.StartDate={'year':Number(enddates[0]),'month':Number(enddates[1]),'day': Number(enddates[2])}
			alert("Please Select Greater date from Start!");
			if(this.userDetail.userType === 'superadmin'){
				this.getWebuserRange()
			}else{
				this.rangeCount();
			}
		}
		else{
			this.api_enddate = event.formatted;
			if(this.userDetail.userType === 'superadmin'){
				this.getWebuserRange()
			}else{
				this.rangeCount();
			}
		}		
	}

	formatDate(date) {
		var d = new Date(date),
		month = '' + (d.getMonth() + 1),
		day = '' + d.getDate(),
		year = d.getFullYear();
		const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;
		return {"format_1":[year, month, day].join('-'),"format_2":[monthNames[d.getMonth()], day, year].join(', ')};
	}

	getWebuserRange(){
		var sendate;
		if(!this.api_startdate){
			this.api_startdate = this.StartDate
		}
		var tomorrow = new Date(this.api_enddate);
		tomorrow.setDate(tomorrow.getDate() + 1);
		var endate = this.formatDate(tomorrow);
		sendate = endate.format_1;
		var startdata = this.api_startdate;
		if(!this.api_enddate){
			sendate = this.EndDate;
		}
		sendate = this.EndDate;
		var rnageVal = 10;
		this.apiService.vendorRange(this.token,this.api_key,startdata.toString(),sendate.toString(),this.activePage.toString(),rnageVal.toString()).subscribe((response:any)=>{
			if(response && response.success == false && response.message == "Token is not valid") {
				this.expireTokenService.expireToken();
			} else if( response && response.results && response.results.length > 0) {
				this.vendorCount = response.results;
			}else{
				this.vendorCount = [];
			}
		},(err)=>{
			console.log(err);
			this.vendorCount = [];
		});
	}

	rangeCount(){
		var sendate;
		if(!this.api_startdate){
			this.api_startdate = this.StartDate
		}
		var tomorrow = new Date(this.api_enddate);
		tomorrow.setDate(tomorrow.getDate() + 1);
		var endate = this.formatDate(tomorrow);
		sendate = endate.format_1;
		var startdata = this.api_startdate;
		if(!this.api_enddate){
			sendate = this.EndDate;
		}
		sendate = this.EndDate;
		var rnageVal = 10;
		this.apiService.userRange(this.token,this.api_key,startdata.toString(),sendate.toString(),this.activePage.toString(),rnageVal.toString(),this.search.toString(),this.searchType.toString()).subscribe((response:any)=>{
			if(response && response.success == false && response.message == "Token is not valid") {
				this.expireTokenService.expireToken();
			} else if( response && response.results && response.results.length > 0) {
				this.countuser = response.results;
			}else{
				this.countuser = [];
			}
		},(err)=>{
			console.log(err);
			this.countuser = [];
		});
	}

	getUserList(){
		this.apiService.getusersList(this.token,this.api_key).subscribe((response:any)=>{
			if(response.success == false && response.message == "Token is not valid") {
				this.expireTokenService.expireToken();
			} else if(response.length > 0) {
				this.val = 5;
				this.userData = response;
			}else{
				this.userData = [];
			}
		},(err)=>{
			console.log(err);
			this.userData = [];
		});
	}

	getProcedure(val){
		return val.replace(/,/g, ", ");
	}

	getDate(val){
		var createDate = new Date(val);
		var dd = String(createDate.getDate()).padStart(2, '0');
		var mm = String(createDate.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = createDate.getFullYear();
		return yyyy + '/' + mm + '/' + dd;
	}

	get_Date(val){
		var createDate = new Date(val);
		var dd = String(createDate.getDate()).padStart(2, '0');
		var mm = String(createDate.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = createDate.getFullYear();
		return mm + '/' + dd + '/' + yyyy;
	}
	
	leadCount(){
		this.apiService.getleads(this.token,this.api_key).subscribe((response:any)=>{
			if(response.success == false && response.message == "Token is not valid") {
				this.expireTokenService.expireToken();
			} else if(response.length > 0 ) {
				this.leadData = response;
			}else{
				this.leadData = [];
			}
		},(err)=>{
			console.log(err);
			this.leadData = [];
		});
	}

	getWebuser(){
		this.apiService.getWebuser(this.userDetail.token,'1','10','','').subscribe((data:any)=>{
			this.webUser= data.results
			this.totalCount = data.totalCount
		},(error)=>{
			console.log(error)
		})
	}

	getNewWebuser(){
		this.apiService.getNewWebuser(this.userDetail.token,this.activePage.toString(),this.range.toString()).subscribe((data:any)=>{
			if(data.results){
				this.newwebUser= data.results
				this.newTotalCount = data.totalCount
			}else{
				this.newTotalCount = 0
				this.newwebUser= []
			}
			
		},(error)=>{
			console.log(error)
		})
	}


	ApiRefresh(refresh){
		this.apiRefresh=refresh
	}

	displayActivePage(activePageNumber:number){  
		this.activePage = activePageNumber;
	}

	changeStatusWebuser(key){
		Swal.fire({
		  title: 'Are you sure?',
		  text: 'You want to approve this vendor!',
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonText: 'Yes, approve it!',
		  cancelButtonText: 'No, keep it'
		}).then((result) => {
		  if (result.value) {
			this.apiService.updateWebuserStatusbyid(this.userDetail.token,key).subscribe((data)=>{
				this.getWebuserRange()
				this.getWebuser() 
			  this.getNewWebuser()
			  Swal.fire(
				'Updated!',
				'Vendor has been approved.',
				'success'
			  )
			},(error)=>{
	
			},()=>{
	
			})
		  // For more information about handling dismissals please visit
		  // https://sweetalert2.github.io/#handling-dismissals
		  } else if (result.dismiss === Swal.DismissReason.cancel) {
			Swal.fire(
			  'Cancelled',
			  'Your record is safe :)',
			  'error'
			)
		  }
		})
	  }

	  pageChanged(data){
		  this.activePage=data
		  this.getNewWebuser()
	  }
	
	  deleteWebuser(key){
		Swal.fire({
		  title: 'Are you sure?',
		  text: 'You want to reject this vendor!',
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonText: 'Yes, reject it!',
		  cancelButtonText: 'No, keep it'
		}).then((result) => {
		  if (result.value) {
			this.apiService.deleteWebuser(this.userDetail.token,key).subscribe((data)=>{
			  this.getWebuserRange()
				this.getWebuser() 
			  this.getNewWebuser()
			  Swal.fire(
				'Reject!',
				'Vendor has been rejected.',
				'success'
			  )
			},(error)=>{
	
			},()=>{
	
			})
		  // For more information about handling dismissals please visit
		  // https://sweetalert2.github.io/#handling-dismissals
		  } else if (result.dismiss === Swal.DismissReason.cancel) {
			Swal.fire(
			  'Cancelled',
			  'Your record is safe :)',
			  'error'
			)
		  }
		})
	  }
	  date_change(date){
		const formatedDate = moment(date).format('MM/DD/YYYY')
		return formatedDate
	  }
}