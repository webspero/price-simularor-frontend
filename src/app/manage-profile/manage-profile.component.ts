import { Component, OnInit, isDevMode } from '@angular/core';
import { ApiServiceService } from '../api-service.service';
import { TokenexpiryService } from '../tokenexpiry.service';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { environment } from '../../environments/environment';
import Swal from 'sweetalert2'

@Component({
	selector: 'app-manage-profile',
	templateUrl: './manage-profile.component.html',
	styleUrls: ['./manage-profile.component.css']
})
export class ManageProfileComponent implements OnInit {
	images;
	selectedItem = 1
	token;
	userRole;
	api_key;
	message;
	CurrentUser;
	user_email;
	doctype;
	editProfileForm: FormGroup;
	successClass;
	submitted;
	module_name;
	profileShow: boolean = true;
	profileimg;
	showloader: boolean = false;
	States = [
		{'id':'1', 'value': 'Alabama'},
		{'id':'2', 'value': 'Alaska'},
		{'id':'3', 'value': 'Arizona'},
		{'id':'4', 'value': 'Arkansas'},
		{'id':'5', 'value': 'California'},
		{'id':'6', 'value': 'Colorado'},
		{'id':'7', 'value': 'Connecticut'},
		{'id':'8', 'value': 'Delaware'},
		{'id':'9', 'value': 'District of Columbia'},
		{'id':'10', 'value': 'Florida'},
		{'id':'11', 'value': 'Georgia'},
		{'id':'12', 'value': 'Hawaii'},
		{'id':'13', 'value': 'Idaho'},
		{'id':'14', 'value': 'Illinois'},
		{'id':'15', 'value': 'Indiana'},
		{'id':'16', 'value': 'Iowa'},
		{'id':'17', 'value': 'Kansas'},
		{'id':'18', 'value': 'Kentucky'},
		{'id':'19', 'value': 'Louisiana'},
		{'id':'20', 'value': 'Maine'},
		{'id':'21', 'value': 'Maryland'},
		{'id':'22', 'value': 'Massachusetts'},
		{'id':'23', 'value': 'Michigan'},
		{'id':'24', 'value': 'Minnesota'},
		{'id':'25', 'value': 'Mississippi'},
		{'id':'26', 'value': 'Missouri'},
		{'id':'27', 'value': 'Montana'},
		{'id':'28', 'value': 'Nebraska'},
		{'id':'29', 'value': 'Nevada'},
		{'id':'30', 'value': 'New Hampshire'},
		{'id':'31', 'value': 'New Jersey'},
		{'id':'32', 'value': 'New Mexico'},
		{'id':'33', 'value': 'New York'},
		{'id':'34', 'value': 'North Carolina'},
		{'id':'35', 'value': 'North Dakota'},
		{'id':'36', 'value': 'Ohio'},
		{'id':'37', 'value': 'Oklahoma'},
		{'id':'38', 'value': 'Oregon'},
		{'id':'39', 'value': 'Pennsylvania'},
		{'id':'40', 'value': 'Rhode Island'},
		{'id':'41', 'value': 'South Carolina'},
		{'id':'42', 'value': 'South Dakota'},
		{'id':'43', 'value': 'Tennessee'},
		{'id':'44', 'value': 'Texas'},
		{'id':'45', 'value': 'Utah'},
		{'id':'46', 'value': 'Vermont'},
		{'id':'47', 'value': 'Virginia'},
		{'id':'48', 'value': 'Washington'},
		{'id':'49', 'value': 'West Virginia'},
		{'id':'50', 'value': 'Wisconsin'},
		{'id':'51', 'value': 'Wyoming'}
	];



	constructor(
		private apiService: ApiServiceService,
		private expireTokenService: TokenexpiryService,
		private router: Router,
		private _location: Location

	) {
		this.token = localStorage.getItem("token");
		this.api_key = localStorage.getItem("apikey");
		
		this.editProfileForm = new FormGroup({
			module_name: new FormControl({value: '', disabled: true}),
			first_name: new FormControl(null, [Validators.required, Validators.minLength(1), Validators.pattern(/^[\w -]*$/)]),
			last_name: new FormControl(null, [Validators.required, Validators.minLength(1), Validators.pattern(/^[\w -]*$/)]),
			name_of_point: new FormControl(null, [Validators.required, Validators.minLength(3), Validators.pattern(/^[\w -]*$/)]),
			email_of_point: new FormControl(null, [Validators.required, Validators.email, Validators.pattern(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)]),
			name_of_point_admin: new FormControl(null, [Validators.required, Validators.minLength(3), Validators.pattern(/^[\w -]*$/)]),
			email_of_point_admin: new FormControl(null, [Validators.required, Validators.email, Validators.pattern(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)]),
			provider_business_email: new FormControl({value: '', disabled: true}, [Validators.required, Validators.email,Validators.pattern(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)]),
			mobile: new FormControl(null, [Validators.minLength(8), Validators.maxLength(15), Validators.pattern(/^[0-9]*$/), Validators.required]),
			user_email: new FormControl(null),
			providerurl: new FormControl(null, Validators.required),
			street_address: new FormControl(null, Validators.required),
			city: new FormControl(null, [Validators.required, Validators.minLength(3)]),
			country: new FormControl('usa', Validators.required),
			state: new FormControl(null, Validators.required),
			zipcode: new FormControl(null, [Validators.required, Validators.minLength(3)])
		});
		this.get_CurrentUser();
	}

	noWhitespaceValidator(control: FormControl) {
		const isWhitespace = (control.value || '').trim().length === 0;
		const isValid = !isWhitespace;
		return isValid ? null : { 'whitespace': true };
	}
	
	get f() {
		return this.editProfileForm.controls;
	}
	
	backClicked() {
		this._location.back();
	}

	ngOnInit() {}

	get_CurrentUser() {
		this.apiService.currentUser(this.token, this.api_key).subscribe((response: any) => {
			if (response.success == false && response.message == "Token is not valid") {
				this.expireTokenService.expireToken();
			} else if (response.length > 0) {
				this.CurrentUser = response[0];
				this.userRole = response[0].userType
				if (response[0].profile_image) {
					this.profileimg = environment.backendUrl + '' + response[0].profile_image;
				} else {
					this.profileimg = environment.backendUrl + 'uploads/assets/img/default.png';
				}
				this.editProfileForm.controls['module_name'].setValue(response[0].surgeon_name, {
					onlySelf: true
				});

				this.editProfileForm.controls['first_name'].setValue(response[0].first_name, {
					onlySelf: true
				});
				this.editProfileForm.controls['last_name'].setValue(response[0].last_name, {
					onlySelf: true
				});
				this.editProfileForm.controls['name_of_point'].setValue(response[0].name_of_point, {
					onlySelf: true
				});
				this.editProfileForm.controls['email_of_point'].setValue(response[0].email_of_point, {
					onlySelf: true
				});
				this.editProfileForm.controls['name_of_point_admin'].setValue(response[0].name_of_point_admin, {
					onlySelf: true
				});
				this.editProfileForm.controls['email_of_point_admin'].setValue(response[0].email_of_point_admin, {
					onlySelf: true
				});
				this.editProfileForm.controls['provider_business_email'].setValue(response[0].provider_business_email, {
					onlySelf: true
				});
				this.editProfileForm.controls['mobile'].setValue(response[0].mobile, {
					onlySelf: true
				});
				this.editProfileForm.controls['user_email'].setValue(response[0].email, {
					onlySelf: true
				});
				this.editProfileForm.controls['providerurl'].setValue(response[0].providerurl, {
					onlySelf: true
				});
				this.editProfileForm.controls['street_address'].setValue(response[0].street_address, {
					onlySelf: true
				});
				this.editProfileForm.controls['city'].setValue(response[0].city, {
					onlySelf: true
				});
				//this.editProfileForm.controls['state'].setValue(response[0].state, {onlySelf: true});
				this.editProfileForm.controls['state'].setValue(response[0].state, {
					onlySelf: true
				});
				
				// this.editProfileForm.controls['counSurgeontry'].setValue(this.editProfileForm.controls['country'].value, {onlySelf: true});
				this.editProfileForm.controls['zipcode'].setValue(response[0].zipcode, {
					onlySelf: true
				});
			} else {
				this.message = "Something went wrong";
			}
		}, (err) => {
			console.log(err);
			this.message = "Something went worng";
		});
	}

	showDiv(val, data) {
		this.selectedItem = data
		this.profileShow = val
	}

	selectImage(event) {
		if (event.target.files.length > 0) {
			const file = event.target.files[0];
			this.images = file;
			var that = this
			var reader = new FileReader();
			reader.onloadend = function() {
				that.profileimg = reader.result
			}
			reader.readAsDataURL(file);
		}
	}

	onSubmit() {
		this.showloader = true;
		const formData = new FormData();
		formData.append('file', this.images);
		//formData.forEach(ele => console.log("formdata==> data", ele))
		this.apiService.uploadUsermeta(this.token, this.api_key,formData).subscribe(
			(res: any) => {
			if (res.status === 200) {
				this.showloader = false;
				this.profileimg = environment.backendUrl + '' + res.results[0].profile_image;
				Swal.fire(
					'Success',
					'Image successfully uploaded',
					'success'
				)
				document.getElementById('tested').click()				
			} else {
				Swal.fire(
					'Error',
					'Error in image uploaded',
					'error'
				)
				this.showloader = false;
				this.profileimg = environment.backendUrl + 'uploads/assets/img/profile.jpg';
			}
		},
		(err) => {
			this.showloader = false;
			console.log(err);
		})
	}

	Save_Edited_Profile(editProfileForm) {
		this.submitted = true;
		if (this.editProfileForm.valid) {
			this.apiService.UpdateUser(editProfileForm.value, this.token, this.api_key).subscribe((response: any) => {
				console.log(response)
				if (response.length > 0) {
					this.CurrentUser = response[0];
					this.message = "User Details Updated";
					this.successClass = 'alert-success';
					Swal.fire(
						'Success!',
						'Successfully updated',
						'success'
					)
					this.submitted = false;
					localStorage.setItem("username", response[0]['first_name'] + ' ' + response[0]['last_name']);
					this.router.navigateByUrl('/RefreshComponent', {
						skipLocationChange: true
					}).then(() => {
						this.router.navigate(['manage-profile']);
					});
					//this.router.navigate(['manage-profile']);
				} else {
					this.message = "Something went worng";
					this.successClass = 'alert-danger';
					Swal.fire(
						'Error!',
						'Error in update profile.',
						'error'
					)
					this.submitted = false;
				}
			}, (err) => {
				this.message = "Something went worng";
				this.successClass = 'alert-danger';
				Swal.fire(
					'Error!',
					'Error in update profile.',
					'error'
				)
				this.submitted = false;
			});
		}else{
			return
		}
	}
}