import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup, Validators} from '@angular/forms'
import { ApiServiceService } from 'src/app/api-service.service';
import { ActivatedRoute } from '@angular/router'
import Swal from 'sweetalert2'
import { Location } from '@angular/common';

@Component({
  selector: 'app-procedure-edit',
  templateUrl: './procedure-edit.component.html',
  styleUrls: ['./procedure-edit.component.css']
})
export class ProcedureEditComponent implements OnInit {

    public gender = [
    {name:"Male"},
    {name:"Female"},
    {name:"Both"}
  ]
  public procedureForm : FormGroup
  public procedureId : any
  public isSubmit : boolean = false
  public submitted : boolean = false
  public userDetail : any
  public doctorType : any =[]
  public procedureCategory : any =[]
  public procedureData : any =[]
  public moduleList : any =[]
  pageno:number=1
  range:number=100

  constructor(private _location: Location,private formbuilder : FormBuilder,private apiService : ApiServiceService, private route : ActivatedRoute) {
    this.procedureForm = this.formbuilder.group({
      procedure_name:['',[Validators.minLength(3), Validators.required]],
      module:[null,Validators.required],
      description:['',[Validators.required]],
      status:[1,Validators.required],
      gender:[null,Validators.required],
      body_part:[null,Validators.required],
      type:['']
    })
   }

  ngOnInit() {
    this.userDetail = JSON.parse(localStorage.getItem('userdetail'))
    this.procedureId = this.route.snapshot.paramMap.get('id')
    this.getProcedureById(this.procedureId)  
    this.getModule()
    this.getProcedureCategory()
    this.getDoctorType()
  }

  editProcedureData(){
    this.procedureForm.patchValue({
      procedure_name:this.procedureData.procedure_name,
      module:this.procedureData.module,
      description:this.procedureData.description,
      status:this.procedureData.status,
      gender:this.procedureData.gender,
      body_part:this.procedureData.body_part,
      type:this.procedureData.type === 'null' ? '' : this.procedureData.type
    })
  }
  
  backClicked() {
    this._location.back();
  }
  submit(){
    if(this.submitted) return
    this.isSubmit=true
    this.submitted=true
    if(this.procedureForm.invalid) return this.submitted=false
    this.apiService.updateProcedureById(this.userDetail.token,this.procedureForm.value,this.procedureId).subscribe((data:any)=>{
      Swal.fire(
        'Success!',
        'Procedure successfully updated',
        'success'
      )
      this.backClicked()
      this.isSubmit=false
      this.submitted=false
    },(error)=>{
      console.log(error)
      this.isSubmit=false
      this.submitted=false
    })
  }

  getProcedureById(id){
    this.apiService.getProcedureById(this.userDetail.token,id).subscribe((data:any)=>{
      this.procedureData=data.response
      if(!data.response.type){
        data.response.type = "null"
      }
     this.procedureForm.controls['type'].setValue(data.response.type, {onlySelf: true});
     this.procedureForm.controls['body_part'].setValue(data.response.body_part, {onlySelf: true});
     this.procedureForm.controls['module'].setValue(data.response.module, {onlySelf: true});
     this.procedureForm.controls['gender'].setValue(data.response.gender, {onlySelf: true});
     this.procedureForm.controls['procedure_name'].setValue(data.response.procedure_name, {onlySelf: true});
     this.procedureForm.controls['description'].setValue(data.response.description, {onlySelf: true});
      this.editProcedureData() 
    },(error)=>{
      console.log(error)
    })
  }

  getModule(){
    this.apiService.getallModule(this.userDetail.token,this.pageno.toString(),this.range.toString()).subscribe((data:any)=>{
      this.moduleList=data.results
    },(error)=>{
      console.log(error)
    })
  }

  getProcedureCategory(){
    this.apiService.getallProcedureCategory(this.userDetail.token,this.pageno.toString(),this.range.toString()).subscribe((data:any)=>{
      this.procedureCategory=data.results
    },(error)=>{
      console.log(error)
    })
  }

  getDoctorType(){
    this.apiService.getallDoctorType(this.userDetail.token,this.pageno.toString(),this.range.toString()).subscribe((data:any)=>{
      this.doctorType=data.results
    },(error)=>{
      console.log(error)
    })
  }
}
