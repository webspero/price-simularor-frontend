import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup, Validators} from '@angular/forms'
import { ApiServiceService } from 'src/app/api-service.service';
import { ActivatedRoute } from '@angular/router'
import Swal from 'sweetalert2'
import { Location } from '@angular/common';

@Component({
  selector: 'app-procedure-add',
  templateUrl: './procedure-add.component.html',
  styleUrls: ['./procedure-add.component.css']
})
export class ProcedureAddComponent implements OnInit {

  public gender = [
    {name:"Male"},
    {name:"Female"},
    {name:"Both"}
  ]
  public procedureForm : FormGroup
  public procedureId : any
  public isSubmit : boolean = false
  public submitted : boolean = false
  public userDetail : any
  public doctorType : any =[]
  public procedureCategory : any =[]
  public procedureData : any =[]
  public moduleList : any =[]
  pageno:number=1
  range:number=100

  constructor(private _location: Location,private formbuilder : FormBuilder,private apiService : ApiServiceService, private route : ActivatedRoute) {
    this.procedureForm = this.formbuilder.group({
      procedure_name:['',[Validators.minLength(3), Validators.required]],
      module:[null,Validators.required],
      description:['',[Validators.required]],
      status:[1,Validators.required],
      gender:[null,Validators.required],
      body_part:[null,Validators.required],
      type:['']
    })
   }

  ngOnInit() {
    this.userDetail = JSON.parse(localStorage.getItem('userdetail'))
    this.procedureId = this.route.snapshot.paramMap.get('id') 
    this.getModule()
    this.getProcedureCategory()
    this.getDoctorType()
  }
  
  backClicked() {
    this._location.back();
  }
  submit(){
    if(this.submitted) return
    this.isSubmit = true
    this.submitted = true
    if(this.procedureForm.invalid) return this.submitted=false
    this.apiService.createprocedure(this.userDetail.token,this.procedureForm.value).subscribe((data:any)=>{
      Swal.fire(
        'Success!',
        'Procedure successfully created',
        'success'
      )
      this.isSubmit = false
      this.submitted=false
      this.backClicked()
    },(error)=>{
      console.log(error)
      this.isSubmit = false
      this.submitted=false
    })
  }


  getModule(){
    this.apiService.getallModule(this.userDetail.token,this.pageno.toString(),this.range.toString()).subscribe((data:any)=>{
      this.moduleList=data.results
    },(error)=>{
      console.log(error)
    })
  }

  getProcedureCategory(){
    this.apiService.getallProcedureCategory(this.userDetail.token,this.pageno.toString(),this.range.toString()).subscribe((data:any)=>{
      this.procedureCategory=data.results
    },(error)=>{
      console.log(error)
    })
  }

  getDoctorType(){
    this.apiService.getallDoctorType(this.userDetail.token,this.pageno.toString(),this.range.toString()).subscribe((data:any)=>{
      this.doctorType=data.results
    },(error)=>{
      console.log(error)
    })
  }
}
