import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../../api-service.service';
import Swal from 'sweetalert2'
import { ExportToCsv } from 'export-to-csv';

 

@Component({
  selector: 'app-procedure-list',
  templateUrl: './procedure-list.component.html',
  styleUrls: ['./procedure-list.component.css']
})
export class ProcedureListComponent implements OnInit {

  public userDetail : any 
  public procedureData :any
  public RealTime :any
  public range : number = 10
  public activePage :number = 1
  public apiRefresh:Boolean = false
  public totalCount:number
  public totalData:any
  public options = { 
		fieldSeparator: ',',
		quoteStrings: '"',
		decimalSeparator: '.',
		showLabels: true, 
		showTitle: false,
		filename:'data_download',
		useTextFile: false,
		useBom: true,
		useKeysAsHeaders: true
  };
  filterModule;
  filterProcedure;

  public csvExporter = new ExportToCsv(this.options);

  constructor(private apiService:ApiServiceService) { }

  ngOnInit() {
    this.userDetail = JSON.parse(localStorage.getItem('userdetail'))
    this.getProcedureList()
  }

  ngDoCheck(){
		if(this.apiRefresh){
			this.getProcedureList()
			this.apiRefresh=false
		}
  }
  
  pageChanged(data){
    this.activePage=data
    this.getProcedureList()
  }


  getProcedureList(){
		this.apiService.getAllProcedureList(this.userDetail.token,this.activePage.toString(),this.range.toString()).subscribe((data:any)=>{
      this.procedureData= data.totalData
      this.RealTime= data.totalData
      this.totalCount = data.totalCount
      this.totalData =data.totalData
		},(error)=>{
			console.log(error)
		},()=>{
    })
  }
  
   deleteProcedure(id){
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to delete this record!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.apiService.deleteProcedurebyid(this.userDetail.token,id).subscribe((data)=>{
          this.getProcedureList() 
          Swal.fire(
            'Deleted!',
            'Your record has been deleted.',
            'success'
          )
        },(error)=>{

        },()=>{

        })
      // For more information about handling dismissals please visit
      // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'Your record is safe :)',
          'error'
        )
      }
    })
  }

  ApiRefresh(refresh){
		this.apiRefresh=refresh
	}

  displayActivePage(activePageNumber:number){  
		this.activePage = activePageNumber;
  }  
  
  formatDate(date) {
		var d = new Date(date),
		month = '' + (d.getMonth() + 1),
		day = '' + d.getDate(),
		year = d.getFullYear();
		const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;
		
		return {"format_1":[year, month, day].join('-'),"format_2":[monthNames[d.getMonth()], day, year].join(', ')};
	}
	 

  getExcel(value){
    if(value){
      var _today = new Date();
      var today = this.formatDate(_today);
      this.options.filename = value+'_' +today.format_1;
      this.csvExporter = new ExportToCsv(this.options)
      var excelData = []
    
      if(this.totalData.length > 0){
        for(var i=0;i<this.totalData.length;i++){
          excelData.push({"S.NO":i+1,"PROCEDURE NAME":this.totalData[i].procedure_name,"CATEGORY NAME":this.totalData[i].category_name,"MODULE NAME":this.totalData[i].module_name,"GENDER":this.totalData[i].gender,"TYPE":this.totalData[i].type})
        }
      }
      if(excelData.length > 0){
        this.csvExporter.generateCsv(excelData);
      }
    }

  }

  selectSearchType(searchType){
    var oldData = this.RealTime;
    var newFilter = [];
    if(this.filterProcedure && this.filterProcedure!=''){
      this.activePage = 1;
      oldData.filter(item => {
        // your filter logic
        
        if(searchType === 'procedure'){
          var oldProd = item.procedure_name;
          oldProd = oldProd.toLowerCase();
          var newProd = this.filterProcedure;
          var decisionCheck = oldProd.search(newProd.toLowerCase());
          if(decisionCheck >= 0){
            newFilter.push(item);
          }
        }
      })
    }else{
      newFilter = oldData;
    }
    this.totalCount = newFilter.length;
    this.procedureData = newFilter;
    }

}
