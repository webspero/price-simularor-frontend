import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiServiceService } from '../api-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm;
  message;
  token;
  show_button: Boolean = false;
  show_eye: Boolean = false;
  successClass;
  isSubmit : Boolean = false
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private apiService: ApiServiceService,
    private http: HttpClient
  ) {
    this.token = localStorage.getItem("token");
    if (this.token) {
      this.router.navigate(['dashboard']);
    }
  }

  ngOnInit() {
    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.email, Validators.pattern("^[A-Za-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),
      password: new FormControl('', [Validators.required]),
    });
  }

  onSubmit(formval) {
    this.isSubmit= true
    if(this.loginForm.invalid) return
    this.apiService.loginUser(formval).subscribe((response: any) => {
      if (response['status'] === 200) {
        var reuslt = response.response;
        var token = reuslt[0]['token'];
        localStorage.setItem("token", token);
        localStorage.setItem("username", reuslt[0]['first_name']+' '+reuslt[0]['last_name']);
        localStorage.setItem("apikey", reuslt[0]['api_key']);
        localStorage.setItem("userdetail",JSON.stringify(reuslt[0]) );
        this.router.navigate(['dashboard']);
        this.message = 'You are login successfully';
        this.successClass = 'alert-success';
        this.isSubmit=false
      } else {
        this.message = response['error'];
        this.successClass = 'alert-danger';
        this.isSubmit=false
      }
    }, (err) => {
      this.message = err.error['error'];
      this.successClass = 'alert-danger';
      this.isSubmit=false
    });
  }

  forgot() {
    this.router.navigate(['forgetpassword']);
  }

  register() {
    this.router.navigate(['register']);
  }
  
  showPassword() {
    this.show_button = !this.show_button;
    this.show_eye = !this.show_eye;
  }

}
