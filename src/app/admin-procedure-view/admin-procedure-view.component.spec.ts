import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminProcedureViewComponent } from './admin-procedure-view.component';

describe('AdminProcedureViewComponent', () => {
  let component: AdminProcedureViewComponent;
  let fixture: ComponentFixture<AdminProcedureViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminProcedureViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminProcedureViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
