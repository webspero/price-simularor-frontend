import { ApiServiceService } from '../../api-service.service';
import { TokenexpiryService } from '../../tokenexpiry.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MustMatch } from '../../register/_helpers/must-match.validator';
import { Location } from '@angular/common';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  token;
  api_key;
  message;
  CurrentUser;
  user_email;
  updatepassword:FormGroup;
  NewPassword;
  ConfirmPassword;
  successClass;
  passwordchnageform;
  CurrentPassword;
  submitted = false;
  constructor(
    private apiService:ApiServiceService,
    private expireTokenService:TokenexpiryService,
    private formBuilder: FormBuilder,
    private router: Router,
    private _location: Location
  ) {
 
    this.updatepassword = this.formBuilder.group({
      CurrentPassword:['', [Validators.required, Validators.minLength(6)]],
      NewPassword: ['', [Validators.required, Validators.minLength(6),Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')]],
      ConfirmPassword: ['', [Validators.required, Validators.minLength(6)]]
    }, {
      validator: MustMatch('NewPassword', 'ConfirmPassword')
    })
    this.token = localStorage.getItem("token");
		this.api_key = localStorage.getItem("apikey");
   }
   get f() {
      return this.updatepassword.controls;
   }
   backClicked() {
    this._location.back();
  }
  ngOnInit() {
    //console.log(this.title)
  }
  
  showPassword(event,CurrentPassword) {
    let target = event.target || event.srcElement || event.currentTarget;
    if(event.target.previousSibling.attributes.type.value == "password"){
      event.target.previousSibling.setAttribute("type","text");
      event.target.className = "fa fa-eye";
    }else{
      event.target.previousSibling.setAttribute("type","password");
      event.target.className = "fa fa-eye-slash";
    }
  }
  ChnagePassword(){
      this.submitted = true;
      if(this.updatepassword.invalid) return
      var values = this.updatepassword.value;
     
      this.apiService.ChangePassword(this.token,this.api_key,values).subscribe((response:any)=>{
        if(response.success == false && response.message == "Token is not valid") {
           this.expireTokenService.expireToken();
           this.submitted = false;
        } else if(response.status == 200) {
            this.message = response.response + ' Please Login again!!';
            this.successClass = 'alert-success';
            localStorage.removeItem("username");
                localStorage.removeItem("apikey");
                localStorage.removeItem("token");
             setTimeout (() => {
                this.router.navigate(['/login']);
              }, 1000);
              this.submitted = false;
        }else{
          this.message = response.error.error;
          this.successClass = 'alert-danger';
          this.submitted = false;
        }
        },(err)=>{
          this.message = err.error.error;
          this.successClass = 'alert-danger';
          this.submitted = false;
        });
    
  }
}