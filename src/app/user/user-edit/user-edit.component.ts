import { Component, OnInit } from '@angular/core';
import { MultiSelectAllModule } from '@syncfusion/ej2-angular-dropdowns';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiServiceService } from '../../api-service.service';
import { TokenexpiryService } from '../../tokenexpiry.service';
import * as $ from 'jquery';
import { Location } from '@angular/common';

@Component({
	selector: 'app-user-edit',
	templateUrl: './user-edit.component.html',
	styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {
	public token;
	public apikey;
	public param1;
	public message;
	public userdata;
	public userForm;
	public userid;
	showloader:boolean=false;
	public user_form_data;
	public datalog=[];
	public selecded_category;
	public customvalues;
	public result = [];
	public catList;

	public userEmail;
	public client_gender;
	public first_name;
	public last_name;
	public user_phone;

	constructor(
		private activatedRoute: ActivatedRoute,
		private http: HttpClient,
		private router: Router,
		private apiService: ApiServiceService,
		private expireTokenService: TokenexpiryService,
		private _location: Location
	) {
		this.token = localStorage.getItem("token");
		this.apikey = localStorage.getItem("apikey");
		this.userid = this.activatedRoute.snapshot.paramMap.get('id');
	}

	ngOnInit() {
		this.dataShow();
	}

	backClicked() {
		this._location.back();
	}

	dataShow(){
		this.showloader = true;
		this.apiService.getuserbyId(this.token,this.apikey,this.userid).subscribe((response:any)=>{
			if (response.success == false && response.message == "Token is not valid") {
				this.expireTokenService.expireToken();
			} else if(response.length > 0){
				this.showloader = false;
				this.userEmail = response[0].user_email;
				this.client_gender = response[0].clientGender;
				this.first_name = response[0].first_name;
				this.last_name = response[0].last_name;
				this.user_phone = response[0].user_phone;
				this.userdata = response[0];
				this.userForm = response;//JSON.parse(this.userdata.form_values);
				this.catList = response.map( (value) => value.category_name).filter( (value, index, _arr) => _arr.indexOf(value) == index);
			}else{
				this.message = "Something went wrong!";
			}
		},(err)=>{
			console.log(err);
			this.message = "Something went wrong!";
		});
	}

	logValue(form: NgForm){
		var datasets = {};
		this.apiService.updateProcedure(datasets,this.token,this.param1,this.apikey).subscribe((response: any) => {
			if (response.success == false && response.message == "Token is not valid") {
				this.expireTokenService.expireToken();

			} else if(response.status == 200){
				//this.userdata = response;
			}else{
				this.message = "Something went wrong!";
			}
		}, (err) => {
			console.log(err);
		})
	}

	getProcedureList(id){
		this.apiService.procedureBy(id,this.token,this.apikey).subscribe((response: any) => {
			if (response.success == false && response.message == "Token is not valid") {
				this.expireTokenService.expireToken();
			} else if(response.status == 200){
				this.datalog.push(response.response[0]);
			}else{
				this.message = "Something went wrong!";
			}
		}, (err) => {
			console.log(err);
		})
	}

}
