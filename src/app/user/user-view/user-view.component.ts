import { Component, OnInit ,TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiServiceService } from '../../api-service.service';
import { TokenexpiryService } from '../../tokenexpiry.service';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { SingleDataSet, Color, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import { ExportToCsv } from 'export-to-csv';
import { Location } from '@angular/common';
import { IMyOptions, IMyDate, IMyDateModel } from 'mydatepicker';
import Swal from 'sweetalert2'
import * as moment from 'moment';

//import { JwPaginationComponent } from 'jw-angular-pagination';

@Component({
	selector: 'app-user-view',
	templateUrl: './user-view.component.html',
	styleUrls: ['./user-view.component.css']
})

export class UserViewComponent implements OnInit {
	public submitted = false;
	public d = new Date();
	public startdate;
	public enddate;
	public today;
	public monday;
	public StartDate;
	public EndDate;
	public api_startdate;
	public api_enddate;
	public message;
	public successClass;
	public items;
	public showloader:boolean=false;
	public download_showloader:boolean=false;
	public modalRef: BsModalRef;
	public totalItems;
	public currentPage;
	public api_key;
	public userData:Array<any>=[];
	public totalData:any
	public token;
	public deleteUserId;
	public showexcel;
	public prodArray = [];
	public registerForm:any= {};
	public mainarray = [];
	public RealTime = [];
	public role;
	public dataVal = [];
	public downlaodExcel= [];
	public filterValue:string='';
	public filterType:string="email";
	public options = { 
		fieldSeparator: ',',
		quoteStrings: '"',
		decimalSeparator: '.',
		showLabels: true, 
		showTitle: false,
		filename:'data_download',
		useTextFile: false,
		useBom: true,
		useKeysAsHeaders: true
	};
	pageOfItems:Array<any>=[];
	activePage:number = 1; 
	totalCount:number = 0; 
	range:number = 10; 
	searchForm : FormGroup;
	public dataExcel = [];
	public csvExporter = new ExportToCsv(this.options);
	public myDatePickerOptions: IMyOptions = {
		dateFormat: 'yyyy-mm-dd',
		showClearDateBtn: false,
		editableDateField: false,
		disableSince: {year: this.d.getFullYear(), month: this.d.getMonth() + 1, day: this.d.getDate() +1}
	};
	public apiRefresh:Boolean = false
	public search : string = ''
	public searchType : string = 'email'
	public selectType =[
		{name:'Email',value:'email'},
		{name:'Procedure',value:'procedure'}
	]
	public disableDate:any;
	
	constructor(
		private modalService: BsModalService,
		private formBuilder: FormBuilder,
		private apiService:ApiServiceService,
		private expireTokenService: TokenexpiryService,
		private _location: Location,
	) {
		
		this.searchForm = this.formBuilder.group({
			search:['']
		})
		this.token = localStorage.getItem("token");
		this.api_key = localStorage.getItem("apikey");
		var srole;
		srole = localStorage.getItem("userdetail");
		this.role = JSON.parse(srole);
		this.registerForm={
			user_name:"",
			user_email:"",
			user_phone:""
		}
		var _monday = new Date();
		var days = ((_monday.getDay() + 7) - 1) % 7;
		_monday.setDate(_monday.getDate() - days);
		this.monday = this.formatDate(_monday);
		this.StartDate = this.monday.format_1;
		var _today = new Date();
		this.today = this.formatDate(_today);
		this.EndDate = this.today.format_1;
		//this.getUserList();
		this.rangeCount();
	}

	StartDateChanged(value){
		this.StartDate = value.formatted;
		var g1 = new Date(value.formatted); 
		var g2 = new Date(this.EndDate);
		if(g1.getTime() <= g2.getTime()){
			var startDate = value.formatted;
			startDate = startDate.split("-");
			var test   = {'year':Number(startDate[0]),'month':Number(startDate[1]),'day': Number(startDate[2])};
			this.disableDate= test
			this.api_startdate = value.formatted;
			this.rangeCount();
		}
		else{
			var startDate;
			 var startDate_format = this.formatDate(g1);
				startDate = startDate_format.format_1;
				startDate = startDate.split("-");
				this.EndDate = {'year':Number(startDate[0]),'month':Number(startDate[1]),'day': Number(startDate[2])};
				this.api_startdate = startDate;
				alert("Please Select Less then date from End!");
				this.rangeCount();
		}
		
	}

	EndDateChanged(event:any){
		this.EndDate = event.formatted;
		var g1 = new Date(this.StartDate); 
		var g2 = new Date(event.formatted);
		if(g1.getTime() > g2.getTime()){
			var enddates;
			var end_dates = this.formatDate(g1);
			enddates = end_dates.format_1;
			this.api_enddate = enddates;
			enddates = enddates.split("-");
			this.EndDate = {'year':Number(enddates[0]),'month':Number(enddates[1]),'day': Number(enddates[2])}	
			alert("Please Select Greater date from Start!");
			this.rangeCount();
		}
		else{
			this.api_enddate = event.formatted;
			this.rangeCount();
		}		
	}
	
	ngOnInit() {
		
	}

	backClicked() {
		this._location.back();
	}

	formatDate(date) {
		var d = new Date(date),
		month = '' + (d.getMonth() + 1),
		day = '' + d.getDate(),
		year = d.getFullYear();
		const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;
		
		return {"format_1":[year, month, day].join('-'),"format_2":[monthNames[d.getMonth()], day, year].join(', ')};
	}
	
	getProcedure(val){
		return val.replace(/,/g, ", ")
	}

	rangeCount(){
		this.showloader = true;
		var sendate;
		this.userData = [];
		this.prodArray = [];
		if(!this.api_startdate){
			this.api_startdate = this.StartDate;
		}
		if(!this.api_enddate){
			this.api_enddate = this.EndDate;
	   }
		var tomorrow = new Date(this.api_enddate);
		tomorrow.setDate(tomorrow.getDate() + 1);
		var endate = this.formatDate(tomorrow);
		sendate = endate.format_1;
		var startdata = this.api_startdate;
		var search = this.searchForm.value.search;
		this.apiService.userRange(this.token,this.api_key,startdata.toString(),sendate.toString(),this.activePage.toString(),this.range.toString(),this.search.toString(),this.searchType.toString()).subscribe((response:any)=>{
			if( response && response.success == false && response.message == "Token is not valid") {
				this.expireTokenService.expireToken();
				this.showloader = false;
			} else if(response && response.results && response.results.length > 0) {
				this.userData = response.results;
				this.totalCount = response.totalcount
				this.totalData = response.totalData;
				this.RealTime = response.totalData;
				this.showloader = false;
				this.selectSearchType();
			}
			else{
				this.userData = [];
				this.totalCount = 0;
				this.totalData = [];
				this.RealTime = [];
				this.showloader = false;
				this.selectSearchType();
			}
		},(err)=>{
			this.showloader = false;
		});
	}

	onChangePage(pageOfItems: Array<any>) {
		this.pageOfItems = pageOfItems;
	}

	getUserList(){
		this.showloader = true;
		this.apiService.getusersList(this.token,this.api_key).subscribe((response:any)=>{
			if (response.success == false && response.message == "Token is not valid") {
			this.expireTokenService.expireToken();
			}else if (response && response.length > 0 ) {
				this.showloader = false;
				this.userData = response;
			}else{
				this.userData = [];
				this.message = "No Leads found";
				this.successClass = 'alert-danger';
			}
		},(err)=>{
			console.log(err);
			this.showloader = false;
		});
	}
	
	viewUser(viewUsertemplate: TemplateRef<any>,data){
		this.modalRef = this.modalService.show(viewUsertemplate);
		this.registerForm.name = data.name;
		this.registerForm.email = data.email;
		this.registerForm.contactNumber = data.phone_number;
	}

	editUser(editUsertemplate: TemplateRef<any>){
		//this.modalRef = this.modalService.show(editUsertemplate);
	}

	deleteUser(deleteUserTemplate: TemplateRef<any>,data) {
		this.deleteUserId = data.id;
		this.modalRef = this.modalService.show(deleteUserTemplate);
	} 

	deleteUserfromlist(){
		var id = {"id":this.deleteUserId}
		this.apiService.deleteUser(this.token,id).subscribe((response:any)=>{
			if(response.status == 200){
				this.modalRef.hide();
				this.rangeCount();
			}
		},(err)=>{
			console.log(err);
		})
	}

	onSubmit() {
		this.submitted = true;
		// stop here if form is invalid
		if (this.registerForm.invalid) {
			return;
		}
		// display form values on success
		this.modalRef.hide();
		alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4));
	}

	getDate(val){
	    var createDate = new Date(val);
	    var dd = String(createDate.getDate()).padStart(2, '0');
	    var mm = String(createDate.getMonth() + 1).padStart(2, '0'); //January is 0!
	    var yyyy = createDate.getFullYear();
	    return yyyy + '/' + mm + '/' + dd;
	}

	get_Date(val){
		var createDate = new Date(val);
		var dd = String(createDate.getDate()).padStart(2, '0');
		var mm = String(createDate.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = createDate.getFullYear();
		return mm + '/' + dd + '/' + yyyy;
	}

	ngDoCheck(){
		if(this.apiRefresh){
			this.rangeCount()
			this.apiRefresh=false
		}
	}

	pageChanged(data){
		this.activePage=data
		this.rangeCount()
	}

	ApiRefresh(refresh){
		this.apiRefresh=refresh
	}

	getExcel(value){
		if(value){
			var _today = new Date();
			this.today = this.formatDate(_today);
			this.options.filename = value+'_' +this.today.format_1;
			this.csvExporter = new ExportToCsv(this.options)
			this.dataExcel = [];
			for(var ks=0;ks<this.totalData.length;ks++){
				var pName = this.getProcedure(this.totalData[ks].procedure_name);
				if(this.role.userType==='superadmin'){
					var vendorNameVal = this.totalData[ks].first_name+' '+this.totalData[ks].last_name;
					this.dataExcel.push({"User Number":ks+1,"Vendor Name":vendorNameVal,"Create Date":this.get_Date(this.totalData[ks].created_at),"First Name": this.totalData[ks].first_name,"Last Name": this.totalData[ks].last_name,"Email Address": this.totalData[ks].user_email,"Procedure of interest ":pName,"Phone": this.totalData[ks].user_phone})
				}else{
					this.dataExcel.push({"User Number":ks+1,"Create Date":this.get_Date(this.totalData[ks].created_at),"Email Address": this.totalData[ks].user_email,"First Name": this.totalData[ks].first_name,"Last Name": this.totalData[ks].last_name,"Procedure of interest":pName,"Phone": this.totalData[ks].user_phone})
				}
			}
			if(this.dataExcel.length > 0){
				this.csvExporter.generateCsv(this.dataExcel);
				this.download_showloader = false;
			}
		}

	}
	
	displayActivePage(activePageNumber:number){  
		this.activePage = activePageNumber;
	}  

	
	selectSearchType(){
		var typeVal = this.filterType
		var oldData = this.RealTime;
		var newFilter = [];
		if(this.filterValue && this.filterValue!=''){
			this.activePage = 1;
			oldData.filter(item => {
				// your filter logic
				if(typeVal === 'email'){
					var oldEmail = item.user_email;
					oldEmail = oldEmail.toLowerCase();
					var new_Prod = this.filterValue;
					var decision = oldEmail.search(new_Prod.toLowerCase());
					if(decision >= 0){
						newFilter.push(item);
					}
				}

				if(typeVal === 'procedure'){
					var oldProd = item.procedure_name;
					oldProd = oldProd.toLowerCase();
					var newProd = this.filterValue;
					var decisionCheck = oldProd.search(newProd.toLowerCase());
					if(decisionCheck >= 0){
						newFilter.push(item);
					}
				}
			})
		}else{
			newFilter = oldData;
		}
		this.totalCount = newFilter.length;
		this.totalData = newFilter;
	  }

	  deleteUserbyId(valId:any){
		Swal.fire({
		  title: 'Are you sure?',
		  text: 'You want to delete this record!',
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonText: 'Yes, delete it!',
		  cancelButtonText: 'No, keep it'
		}).then((result) => {
		  if (result.value) {
			var id = {"id":valId}
			this.apiService.deleteUser(this.token,id).subscribe((data:any)=>{
			  this.rangeCount() 
			  Swal.fire(
				'Deleted!',
				'Your record has been deleted.',
				'success'
			  )
			},(error)=>{
	
			},()=>{
	
			})
		  // For more information about handling dismissals please visit
		  // https://sweetalert2.github.io/#handling-dismissals
		  } else if (result.dismiss === Swal.DismissReason.cancel) {
			Swal.fire(
			  'Cancelled',
			  'Your record is safe :)',
			  'error'
			)
		  }
		})
	  }
	
	  date_change(date){
		const formatedDate = moment(date).format('MM/DD/YYYY')
		return formatedDate
	  }

}
