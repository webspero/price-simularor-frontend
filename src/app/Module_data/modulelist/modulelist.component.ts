import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms'
import { ApiServiceService } from '../../api-service.service';
import Swal from 'sweetalert2'
import * as moment from 'moment';


@Component({
  selector: 'app-modulelist',
  templateUrl: './modulelist.component.html',
  styleUrls: ['./modulelist.component.css']
})
export class ModulelistComponent implements OnInit {

  public userDetail : any 
  public moduleList :any
  public range : number = 10
  public activePage :number = 1
  public apiRefresh:Boolean = false
  public totalCount:number
  public doctorList :any
  public docrange : number = 500
  public moduleForm : FormGroup
  public moduleId : any
  public isSubmit : boolean = false
  public submitted : boolean = false;
  public popupSub:boolean=false
  constructor(private apiService:ApiServiceService,private formbuilder:FormBuilder) { }

  ngOnInit() {
    this.userDetail = JSON.parse(localStorage.getItem('userdetail'))
    this.getModuleList() 
    this.getDoctorList()
    this.moduleForm = this.formbuilder.group({
      doc_id:['',Validators.required],
      module_name : ['',[Validators.required]]
    })
  }

  ngDoCheck(){
		if(this.apiRefresh){
			this.getModuleList()
			this.apiRefresh=false
		}
  }

  pageChanged(data){
    this.activePage=data
    this.getModuleList()
  }

  get_Date(val){
		var createDate = new Date(val);
		var dd = String(createDate.getDate()).padStart(2, '0');
		var mm = String(createDate.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = createDate.getFullYear();
		return mm + '/' + dd + '/' + yyyy;
  }
  
  getDoctorList(){
		this.apiService.getallDoctorType(this.userDetail.token,this.activePage.toString(),this.docrange.toString()).subscribe((data:any)=>{
      this.doctorList= data.results
		},(error)=>{
			console.log(error)
		},()=>{
    })
  }
  
  getModuleList(){
		this.apiService.getallModule(this.userDetail.token,this.activePage.toString(),this.range.toString()).subscribe((data:any)=>{
      this.moduleList= data.results
      this.totalCount = data.totalCount
		},(error)=>{
			console.log(error)
		},()=>{
    })
  }
  
   deletemodule(id){
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to delete this record!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.apiService.deleteModulebyid(this.userDetail.token,id).subscribe((data)=>{
          this.getModuleList() 
          Swal.fire(
            'Deleted!',
            'Your record has been deleted.',
            'success'
          )
        },(error)=>{

        },()=>{

        })
      // For more information about handling dismissals please visit
      // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'Your record is safe :)',
          'error'
        )
      }
    })
  }

  ApiRefresh(refresh){
		this.apiRefresh=refresh
	}

  displayActivePage(activePageNumber:number){  
		this.activePage = activePageNumber;
  }  
  
  addModule(){
    this.moduleId=""
    this.popupSub=false;
    this.moduleForm.controls['doc_id'].setValue('');
    this.moduleForm.controls['module_name'].setValue('');
  }

  editmodule(id){
    this.moduleId = id
    this.apiService.getModuleById(this.userDetail.token,id).subscribe((data : any)=>{
      this.moduleForm.patchValue({
        doc_id:data && data.results && data.results.doc_id,
        module_name:data && data.results && data.results.module_name
      })
    },(error)=>{
      console.log(error)
    })
  }

  submit(){
    if(this.submitted) return
    this.isSubmit= true
    this.popupSub= true
    this.submitted=true
    if(this.moduleForm.invalid) return this.submitted=false
    if(this.moduleId){
      // edit
      this.apiService.updatemodule(this.userDetail.token,this.moduleId,this.moduleForm.value).subscribe((data)=>{
        this.isSubmit= false
        this.submitted=false
        this.getModuleList()
        document.getElementById('close').click()
      },(error)=>{
        this.isSubmit= false
        this.submitted=false
        console.log(error)
      },()=>{
        this.isSubmit= false
        this.submitted=false
      })
    }else{
      // add
      this.apiService.createmodule(this.userDetail.token,this.moduleForm.value).subscribe((data)=>{
        this.isSubmit=false
        this.submitted=false
        this.getModuleList()
        document.getElementById('close').click()
      },(error)=>{
        console.log(error)
        this.isSubmit=false
        this.submitted=false
      },()=>{
        this.isSubmit=false
        this.submitted=false
      })
    }
  }

  date_change(date){
		const formatedDate = moment(date).format('MM/DD/YYYY')
		return formatedDate
	  }
}
