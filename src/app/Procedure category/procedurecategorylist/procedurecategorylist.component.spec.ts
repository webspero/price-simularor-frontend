import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcedurecategorylistComponent } from './procedurecategorylist.component';

describe('ProcedurecategorylistComponent', () => {
  let component: ProcedurecategorylistComponent;
  let fixture: ComponentFixture<ProcedurecategorylistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedurecategorylistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedurecategorylistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
