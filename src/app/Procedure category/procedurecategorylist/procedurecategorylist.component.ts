import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../../api-service.service';
import {FormGroup, FormBuilder, Validators, FormControl} from '@angular/forms'
import Swal from 'sweetalert2'
import * as moment from 'moment';

@Component({
  selector: 'app-procedurecategorylist',
  templateUrl: './procedurecategorylist.component.html',
  styleUrls: ['./procedurecategorylist.component.css']
})
export class ProcedurecategorylistComponent implements OnInit {

  public userDetail : any 
  public moduleList :any
  public procedureCategoryList :any
  public range : number = 10
  public activePage :number = 1
  public apiRefresh:Boolean = false
  public totalCount:number
  public procedureCategoryForm : FormGroup
  public procedureCategoryId : any
  public isSubmit : boolean = false
  public submitted : boolean = false
  public popupSub:boolean=false
  constructor(private apiService:ApiServiceService, private formbuilder : FormBuilder) { }

  ngOnInit() {
    this.userDetail = JSON.parse(localStorage.getItem('userdetail'))
    this.getProcedureCategory() 
    this.getModuleList() 
    this.procedureCategoryForm = this.formbuilder.group({
      module_id:['',Validators.required],
      category_name : ['',[Validators.required]]
    })
  }

  noWhitespaceValidator(control: FormControl) {
		const isWhitespace = (control.value || '').trim().length === 0;
		const isValid = !isWhitespace;
		return isValid ? null : { 'whitespace': true };
  }
  
  ngDoCheck(){
		if(this.apiRefresh){
			this.getProcedureCategory()
			this.apiRefresh=false
		}
  }
  
  pageChanged(data){
    this.getProcedureCategory()
  }
  
  getProcedureCategory(){
		this.apiService.getallProcedureCategory(this.userDetail.token,this.activePage.toString(),this.range.toString()).subscribe((data:any)=>{
      this.procedureCategoryList= data.results
      this.totalCount = data.totalCount
		},(error)=>{
			console.log(error)
		},()=>{
    })
  }
  
   deleteProcedure(id){
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to delete this record!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.apiService.deleteProcedureCategoryById(this.userDetail.token,id).subscribe((data)=>{
          this.getProcedureCategory() 
          Swal.fire(
            'Deleted!',
            'Your record has been deleted.',
            'success'
          )
        },(error)=>{

        },()=>{

        })
      // For more information about handling dismissals please visit
      // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'Your record is safe :)',
          'error'
        )
      }
    })
  }

  ApiRefresh(refresh){
		this.apiRefresh=refresh
	}

  displayActivePage(activePageNumber:number){  
		this.activePage = activePageNumber;
  }  

  get_Date(val){
		var createDate = new Date(val);
		var dd = String(createDate.getDate()).padStart(2, '0');
		var mm = String(createDate.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = createDate.getFullYear();
		return mm + '/' + dd + '/' + yyyy;
	}
  
  getModuleList(){
		this.apiService.getallModule(this.userDetail.token,this.activePage.toString(),this.range.toString()).subscribe((data:any)=>{
      this.moduleList= data.results
      // this.totalCount = data.totalCount
		},(error)=>{
			console.log(error)
		},()=>{
    })
  }
  

  addProcedureCategory(){
    this.procedureCategoryId=""
    this.popupSub=false;
    this.procedureCategoryForm.controls['module_id'].setValue('');
    this.procedureCategoryForm.controls['category_name'].setValue('');

  }

  editProcedureCategory(id){
    this.procedureCategoryId = id
    this.apiService.getProcedureCategoryById(this.userDetail.token,id).subscribe((data : any)=>{
      this.procedureCategoryForm.patchValue({
        module_id:data && data.results && data.results.module_id,
        category_name:data && data.results && data.results.category_name
      })
    },(error)=>{
      console.log(error)
    })
  }

  submit(){
    if(this.submitted) return
    this.isSubmit= true
    this.submitted=true
    this.popupSub=true
    if(this.procedureCategoryForm.invalid) return this.submitted=false
    if(this.procedureCategoryId){
      // edit
      this.apiService.updateProcedureCategory(this.userDetail.token,this.procedureCategoryId,this.procedureCategoryForm.value).subscribe((data)=>{
        this.isSubmit= false
        this.submitted=false
        this.getProcedureCategory()
        document.getElementById('close').click()
      },(error)=>{
        this.isSubmit= false
        this.submitted=false
        console.log(error)
      },()=>{
        this.isSubmit= false
        this.submitted=false
      })
    }else{
      // add
      this.apiService.createProcedureCategory(this.userDetail.token,this.procedureCategoryForm.value).subscribe((data)=>{
        this.isSubmit=false
        this.submitted=false
        this.getProcedureCategory()
        document.getElementById('close').click()
      },(error)=>{
        console.log(error)
        this.isSubmit=false
        this.submitted=false
      },()=>{
        this.isSubmit=false
        this.submitted=false
      })
    }
  }

  date_change(date){
		const formatedDate = moment(date).format('MM/DD/YYYY')
		return formatedDate
	  }

}
