import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { AuthService } from './auth/auth.service';
import * as decode  from 'jwt-decode';

@Injectable({
	providedIn: 'root'
})
export class RoleGuardService implements CanActivate {
	
	constructor(
		public auth: AuthService,
		public router: Router
	) {}
	
	canActivate(route: ActivatedRouteSnapshot): boolean {
		
		let expectedRoleArray = route.data;
		expectedRoleArray = expectedRoleArray.expectedRole;
		
		const token = localStorage.getItem('token');
		if(token){

			// decode the token to get its payload
			const tokenPayload = decode(token);
			let  expectedRole = '';

			for(let i=0; i<expectedRoleArray.length; i++){
				if(expectedRoleArray[i]==tokenPayload.userrole){
					expectedRole = tokenPayload.userrole;
				}
			}
			
			if (this.auth.isLoggedIn() && tokenPayload.userrole == expectedRole) {
				return true;
			}
			return false;
		}else{
			localStorage.clear();
			this.router.navigate['login'];
		}
	}
}