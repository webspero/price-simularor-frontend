import { Component, OnInit, TemplateRef, Pipe, PipeTransform } from '@angular/core';
import { Router } from '@angular/router';
import { ApiServiceService } from '../api-service.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { TokenexpiryService } from '../tokenexpiry.service';
import { JsonAdaptor } from '@syncfusion/ej2-data';
import { Location } from '@angular/common';
import Swal from 'sweetalert2'

@Component({
	selector: 'app-add-view',
	templateUrl: './add-view.component.html',
	styleUrls: ['./add-view.component.css']
})
export class AddViewComponent implements OnInit {
	token;
	apikey;
	procedure_array: any = [];
	RealTime: any = [];
	allModule;
	reply;
	updateDiscount: FormGroup;
	modalRef: BsModalRef;
	isSubmit: boolean = false
	activePage: number = 1
	range: number = 10
	totalCount: number
	apiRefresh: boolean = false
	showPop: boolean = false
	userDetail:any;
	filterModule;
	filterProcedure;
	messageShow;

	constructor(
		private router: Router,
		private modalService: BsModalService,
		private apiService: ApiServiceService,
		private formBuilder: FormBuilder,
		private expireTokenService: TokenexpiryService,
		private _location: Location
	) {
		this.userDetail = JSON.parse(localStorage.getItem('userdetail')) 
		this.token = localStorage.getItem("token");
		this.apikey = localStorage.getItem("apikey");
		this.getProcedureList();
		this.moduleList();
		this.updateDiscount = this.formBuilder.group({
			discount: ['', [Validators.minLength(0), Validators.maxLength(100), Validators.pattern(/^[0-9]*$/), Validators.required]],
		})
	}

	ngOnInit() {
		this.getDiscount()
	}

	getDiscount() {
		this.apiService.currentUser(this.token, this.apikey).subscribe((response: any) => {
			if (response.success == false && response.message == "Token is not valid") {
				this.expireTokenService.expireToken();
			} else if (response.length > 0) {
				this.updateDiscount.patchValue({
					discount: response[0].discount_rate
				})
				// this.updateDiscount.controls['discount'].setValue(response[0].discount_rate);
			}
		}, (err) => {
			console.log(err);
		})
	}

	ngDoCheck() {
		if (this.apiRefresh) {
			this.getProcedureList()
			this.apiRefresh = false
		}
	}

	pageChanged(data){
		this.activePage=data
		this.getProcedureList()
	  }

	backClicked() {
		this._location.back();
	}

	getProcedureList() {
		this.apiService.getAllProcedureListAdmin(this.token, this.activePage.toString(), this.range.toString(),this.userDetail.id.toString(),this.userDetail.doctype.toString()).subscribe((response: any) => {
			if (response.success == false && response.message == "Token is not valid") {
				this.expireTokenService.expireToken();
			} else if (response.status == 200) {
				this.procedure_array = response.totalData;
				this.RealTime = response.totalData;
				this.totalCount = response.totalCount
			}
		}, (err) => {
			console.log(err);
		})
	}

	rate_show(values){
		var explodeVal = values.split(",");
		var maleCost = "For Male USD $"+explodeVal[0];
		var femaleCost = "For Female USD $"+explodeVal[1];
		return maleCost+"<br>"+femaleCost;
	}

	editProcedure(array) {
		var arrayData = JSON.stringify(array);
		this.router.navigate(['addProcedure']);
	}

	addProcedure() {
		this.router.navigate(['addProcedure']);
	}

	moduleList(value = 'all') {
		this.apiService.getModule(this.token, this.apikey, value).subscribe((response: any) => {
			this.allModule = response;
		}, (err) => {
			console.log(err);
		});
	}

	modulebyId(val) {
		if (this.allModule) {
			for (var k = 0; k < this.allModule.length; k++) {
				if (this.allModule[k].id == val) {
					return this.allModule[k].module_name;
				}
			}
		}
	}

	deleteProcedures(id_val) {
		var id = { id_val };
		if (confirm('Are you sure you want to delete this?')) {
			this.apiService.deleteProcedure(id, this.token).subscribe((response: any) => {
				if (response.success == false && response.message == "Token is not valid") {
					this.expireTokenService.expireToken();
				} else {
					if (response.status == 200) {
						this.getProcedureList();
					}
				}
			}, (err) => {
				console.log(err);
			})
		}

	}

	deleteProcedure(id){
		Swal.fire({
		  title: 'Are you sure?',
		  text: 'You want to delete this record!',
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonText: 'Yes, delete it!',
		  cancelButtonText: 'No, keep it'
		}).then((result) => {
		  if (result.value) {
			this.apiService.deleteProcedurebyid(this.userDetail.token,id).subscribe((data)=>{
			  this.getProcedureList() 
			  Swal.fire(
				'Deleted!',
				'Your record has been deleted.',
				'success'
			  )
			},(error)=>{
				console.log(error)
			},()=>{
	
			})
		  // For more information about handling dismissals please visit
		  // https://sweetalert2.github.io/#handling-dismissals
		  } else if (result.dismiss === Swal.DismissReason.cancel) {
			Swal.fire(
			  'Cancelled',
			  'Your record is safe :)',
			  'error'
			)
		  }
		})
	  }
	

	discountForm(disocuntForm: TemplateRef < any > ) {
		this.modalRef = this.modalService.show(disocuntForm);
	}

	submitDiscount(value) {
		this.isSubmit = true
		if (this.updateDiscount.invalid) return
		this.apiService.discount(this.token, this.apikey, value).subscribe((response: any) => {
			this.updateDiscount.controls['discount'].setValue(response.response);
			this.reply = "Discount rate Updated";
			this.modalRef.hide();
			this.reply = '';
			this.isSubmit = false
			this.getDiscount()
		}, (err) => {
			console.log(err);
			this.isSubmit = false
		});
	}

	ApiRefresh(refresh) {
		this.apiRefresh = refresh
	}

	displayActivePage(activePageNumber: number) {
		this.activePage = activePageNumber;
	}

	//Search Filter
	selectSearchType(searchType){
		var oldData = this.RealTime;
		var newFilter = [];
		if(this.filterProcedure && this.filterProcedure!=''){
			this.activePage = 1;
			oldData.filter(item => {
				// your filter logic
				
				if(searchType === 'procedure'){
					var oldProd = item.procedure_name;
					oldProd = oldProd.toLowerCase();
					var newProd = this.filterProcedure;
					var decisionCheck = oldProd.search(newProd.toLowerCase());
					if(decisionCheck >= 0){
						newFilter.push(item);
					}
				}
			})
		}else{
			newFilter = oldData;
		}
		
		this.totalCount = newFilter.length;
		this.procedure_array = newFilter;
	  }
}