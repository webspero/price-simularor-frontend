import { TestBed } from '@angular/core/testing';

import { TokenexpiryService } from './tokenexpiry.service';

describe('TokenexpiryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TokenexpiryService = TestBed.get(TokenexpiryService);
    expect(service).toBeTruthy();
  });
});
