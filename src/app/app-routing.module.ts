import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChartsModule } from 'ng2-charts';
import { AuthGuard } from './auth.guard';

import { LoginComponent } from './login/login.component';
import { ForgetpasswordComponent } from './forgetpassword/forgetpassword.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserCreateComponent } from './users/user-create/user-create.component';
import { UserViewComponent } from './user/user-view/user-view.component';
import { AddUserComponent } from './add-user/add-user.component';
import { AddProcedureComponent } from './add-procedure/add-procedure.component';
import { AddViewComponent } from './add-view/add-view.component';
import { EditProcedureComponent } from './edit-procedure/edit-procedure.component';
import { UserEditComponent } from './user/user-edit/user-edit.component';
import { SettingsComponent } from './settings/settings.component';
import { ManageProfileComponent } from './manage-profile/manage-profile.component';
import { ChangePasswordComponent } from './user/change-password/change-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ViewComponent } from './webuser/view/view.component';
import { ListComponent } from './webuser/list/list.component';
import { EditComponent } from './webuser/edit/edit.component';
import { ProcedureListComponent } from './Procedure/procedure-list/procedure-list.component';
import { ProcedureEditComponent } from './Procedure/procedure-edit/procedure-edit.component';
import { DoctorTypeListComponent } from './Doctor_type/doctor-type-list/doctor-type-list.component';
import { ModulelistComponent } from './Module_data/modulelist/modulelist.component';
import { ProcedurecategorylistComponent } from './Procedure category/procedurecategorylist/procedurecategorylist.component';
import { ProcedureAddComponent } from './Procedure/procedure-add/procedure-add.component';
import { VendorProcedureListComponent } from './VendorProcedure/vendor-procedure-list/vendor-procedure-list.component';
import { UserdetailsComponent } from './userdetails/userdetails.component';
import { RoleGuardService as RoleGuard } from './role-guard.service';
import { DisclaimerComponent } from './add-procedure/disclaimer/disclaimer.component';
import { VendorprocedureeditComponent } from './VendorProcedure/vendorprocedureedit/vendorprocedureedit.component';
import { AdminProcedureViewComponent } from './admin-procedure-view/admin-procedure-view.component';


const routes: Routes = [
	{ path: '', component: LoginComponent },
	{ path: 'login', component: LoginComponent },
	{ path: 'forgetpassword', component: ForgetpasswordComponent },
	{ path: 'reset-password/:key', component: ResetPasswordComponent },
	{ path: 'register', component: RegisterComponent },
	{ path: 'userdetails/:key', component: UserdetailsComponent },
	{ path: 'home', component: HomeComponent,canActivate: [RoleGuard], data: { expectedRole: ['admin', 'superadmin'] }},
	{ path: 'header', component: HeaderComponent,canActivate: [RoleGuard], data: { expectedRole: ['admin', 'superadmin'] }},
	{ path: 'dashboard', component: DashboardComponent,canActivate: [RoleGuard], data: { expectedRole: ['admin', 'superadmin'] }},
	{ path: 'users', component: UserCreateComponent,canActivate: [RoleGuard], data: { expectedRole: ['admin', 'superadmin'] }},
	{ path: 'user', component: UserViewComponent,canActivate: [RoleGuard], data: { expectedRole: ['admin', 'superadmin'] }},
	{ path: 'addUser', component: AddUserComponent,canActivate: [RoleGuard], data: { expectedRole: ['admin', 'superadmin'] }},
	{ path: 'addProcedure', component: AddProcedureComponent,canActivate: [RoleGuard], data: { expectedRole: ['admin', 'superadmin'] }},
	{ path: 'viewprocedure/:id', component: AdminProcedureViewComponent,canActivate: [RoleGuard], data: { expectedRole: ['admin', 'superadmin'] }},
	{ path: 'vendorProcedure', component: VendorProcedureListComponent,canActivate: [RoleGuard], data: { expectedRole: ['superadmin'] }},	
	{ path: 'view', component: AddViewComponent,canActivate: [RoleGuard], data: { expectedRole: ['admin', 'superadmin'] }},
	{ path: 'disclaimer', component: DisclaimerComponent,canActivate: [RoleGuard], data: { expectedRole: ['admin', 'superadmin'] }},
	{ path: 'view/:id', component: EditProcedureComponent,canActivate: [RoleGuard], data: { expectedRole: ['admin', 'superadmin'] }},
	{ path: 'procedureAdd', component: ProcedureAddComponent,canActivate: [RoleGuard], data: { expectedRole: ['admin', 'superadmin'] }},
	{ path: 'editprocedure/:id', component: VendorprocedureeditComponent,canActivate: [RoleGuard], data: { expectedRole: ['admin', 'superadmin'] }},
	{ path: 'edituser/:id', component: UserEditComponent,canActivate: [RoleGuard], data: { expectedRole: ['admin', 'superadmin'] }},
	{ path: 'settings', component: SettingsComponent,canActivate: [RoleGuard], data: { expectedRole: ['admin', 'superadmin'] }},	
	{ path: 'manage-profile', component: ManageProfileComponent,canActivate: [RoleGuard], data: { expectedRole: ['admin', 'superadmin'] }},	
	{ path: 'change-password', component: ChangePasswordComponent,canActivate: [RoleGuard], data: { expectedRole: ['admin', 'superadmin'] }},
	{ path: 'webuser/:key', component: ViewComponent,canActivate: [RoleGuard], data: { expectedRole: ['superadmin'] }},	
	{ path: 'webuser', component: ListComponent,canActivate: [RoleGuard], data: { expectedRole: ['superadmin'] }},	
	{ path: 'webuseredit/:key', component: EditComponent,canActivate: [RoleGuard], data: { expectedRole: ['superadmin'] }},
	{ path: 'procedure', component: ProcedureListComponent,canActivate: [RoleGuard], data: { expectedRole: ['admin', 'superadmin'] }},
	{ path: 'procedureedit/:id', component: ProcedureEditComponent,canActivate: [RoleGuard], data: { expectedRole: [ 'superadmin'] }},
	{ path: 'doctortype', component: DoctorTypeListComponent,canActivate: [RoleGuard], data: { expectedRole: ['superadmin'] }},
	{ path: 'module', component: ModulelistComponent,canActivate: [RoleGuard], data: { expectedRole: [ 'superadmin'] }},	
	{ path: 'procedurecategory', component: ProcedurecategorylistComponent,canActivate: [RoleGuard], data: { expectedRole: ['admin', 'superadmin'] }},		
	{ path: '**', redirectTo: '', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes )],
  exports: [RouterModule]
})
export class AppRoutingModule { }